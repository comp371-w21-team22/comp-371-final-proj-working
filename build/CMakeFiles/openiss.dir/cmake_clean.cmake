file(REMOVE_RECURSE
  "CMakeFiles/openiss.dir/OpenISS.cpp.o"
  "CMakeFiles/openiss.dir/OpenISS.cpp.o.d"
  "CMakeFiles/openiss.dir/Pipeline.cpp.o"
  "CMakeFiles/openiss.dir/Pipeline.cpp.o.d"
  "CMakeFiles/openiss.dir/VFX.cpp.o"
  "CMakeFiles/openiss.dir/VFX.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIAligner.cpp.o"
  "CMakeFiles/openiss.dir/core/OIAligner.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIDataFrame.cpp.o"
  "CMakeFiles/openiss.dir/core/OIDataFrame.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIDevice.cpp.o"
  "CMakeFiles/openiss.dir/core/OIDevice.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIDeviceFactory.cpp.o"
  "CMakeFiles/openiss.dir/core/OIDeviceFactory.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIFace.cpp.o"
  "CMakeFiles/openiss.dir/core/OIFace.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIFaceAdapterFactory.cpp.o"
  "CMakeFiles/openiss.dir/core/OIFaceAdapterFactory.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIGestureData.cpp.o"
  "CMakeFiles/openiss.dir/core/OIGestureData.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIGestureTracker.cpp.o"
  "CMakeFiles/openiss.dir/core/OIGestureTracker.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIHandData.cpp.o"
  "CMakeFiles/openiss.dir/core/OIHandData.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OINullDevice.cpp.o"
  "CMakeFiles/openiss.dir/core/OINullDevice.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OINullFaceTracker.cpp.o"
  "CMakeFiles/openiss.dir/core/OINullFaceTracker.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OINullGestureTracker.cpp.o"
  "CMakeFiles/openiss.dir/core/OINullGestureTracker.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OINullSkeletonTracker.cpp.o"
  "CMakeFiles/openiss.dir/core/OINullSkeletonTracker.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OISkeleton.cpp.o"
  "CMakeFiles/openiss.dir/core/OISkeleton.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OISkeletonTrackerFactory.cpp.o"
  "CMakeFiles/openiss.dir/core/OISkeletonTrackerFactory.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OITracker.cpp.o"
  "CMakeFiles/openiss.dir/core/OITracker.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIUser.cpp.o"
  "CMakeFiles/openiss.dir/core/OIUser.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIUtils.cpp.o"
  "CMakeFiles/openiss.dir/core/OIUtils.cpp.o.d"
  "CMakeFiles/openiss.dir/core/OIViewer.cpp.o"
  "CMakeFiles/openiss.dir/core/OIViewer.cpp.o.d"
  "CMakeFiles/openiss.dir/viewers/GL2Viewer.cpp.o"
  "CMakeFiles/openiss.dir/viewers/GL2Viewer.cpp.o.d"
  "lib/libopeniss.pdb"
  "lib/libopeniss.so"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/openiss.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
