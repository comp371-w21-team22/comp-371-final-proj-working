#ifndef OPENISS_NULL_FACE_TRACKER_H
#define OPENISS_NULL_FACE_TRACKER_H

#include <memory>
#include <list>

#include "OIFaceAdapter.hpp"
#include "OIClassifier.hpp"

using std::shared_ptr;

namespace openiss
{

class OINullFaceTracker : public openiss::OIFaceAdapter
{
public:
    explicit OINullFaceTracker(OIDevice& pDev);
    ~OINullFaceTracker();

#ifdef OPENISS_OPENCV_SUPPORT
    void drawSingleFaceOnImg(cv::Mat& img) override;
    void drawMultiFacesOnImg(cv::Mat& img) override;
#endif

    void resetAdapterParams() override;
    void initializeFace(openiss::OIFace& p_oFace) override;


private:
    // disable copy constructor and copy assignment
    OINullFaceTracker(const OINullFaceTracker& pAdapter);
    OINullFaceTracker& operator=(OINullFaceTracker&);

    // sign for Adapter params setup
    bool mSingleFaceParams = false;
    bool mMultiFacesParams = false;

    // parameters for single face adapter
    //LandmarkDetector::FaceModelParameters* singleFacialLandmarksParameters;
    //LandmarkDetector::CLNF* singleFacialLandmarksFaceModel;

    // parameters for multi face adapter
    //LandmarkDetector::FaceModelParameters* multiFacialLandmarksParameters;
    //vector<LandmarkDetector::FaceModelParameters> multiFacialLandmarksParametersVector;
    //vector<LandmarkDetector::CLNF> multiFacialLandmarksFaceModelVector;
    vector<bool> active_models;
    int frame_count;

    // FER2D facial expression detect
    //openiss::FER2D* classifier;
    OIClassifier* classifier;


}; // end of class OINullFaceTracker

} // end of namespace

#endif // OPENISS_NULL_FACE_TRACKER_H
