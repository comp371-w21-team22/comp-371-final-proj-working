//
// Created by Haotao Lai on 2018-08-01.
//

#include "OIFXWebCam.hpp"

//GLOBALS
#define NORMAL_MODE 0
#define DEPTH_MODE 1
#define IR_MODE 2

openiss::OIFXWebCam::OIFXWebCam()
{ }

openiss::OIFXWebCam::~OIFXWebCam() {
}

void openiss::OIFXWebCam::init() {

}

void openiss::OIFXWebCam::close() {
    
}

void openiss::OIFXWebCam::open() {
    camera.open(0);
    if (!camera.isOpened()) {
        std::cerr << "ERROR: Could not open camera" << std::endl;
    }
}

bool openiss::OIFXWebCam::enableColor() {
}

bool openiss::OIFXWebCam::enableDepth() {
}

bool openiss::OIFXWebCam::enableRegistered() {
}

bool openiss::OIFXWebCam::enable() {
}

openiss::OIFrame* openiss::OIFXWebCam::readFrame(openiss::StreamType typeFrame) {

}

cv::Mat openiss::OIFXWebCam::getFrame(int mode) {

    // get webcam frame
    camera >> frame;

    cv::Mat greyMat;
    switch(mode){
        case NORMAL_MODE:
            {
                return frame;
            }

        case DEPTH_MODE:
            {
                return frame;
            }

        case IR_MODE:
            {
                greyMat = cv::Mat(frame.rows, frame.cols, CV_8UC3);
                cv::cvtColor(frame, greyMat, CV_BGR2GRAY);
                cv::Mat irImage;
                cv::applyColorMap(greyMat, irImage, cv::COLORMAP_HSV);
                return irImage;
            }

        default:
            {
                return frame;
            }
    }
}

void* openiss::OIFXWebCam::rawDevice() {
}

openiss::Intrinsic openiss::OIFXWebCam::getIntrinsic(openiss::StreamType streamType) {
    return openiss::Intrinsic();
}

openiss::Extrinsic openiss::OIFXWebCam::getExtrinsic(openiss::StreamType from, openiss::StreamType to) {
    return openiss::Extrinsic();
}

float openiss::OIFXWebCam::getDepthScale() {
    return 0;
}
