//
// Created by jash on 20/01/19.
//

#include "OINuiTrackGestureTracker.hpp"

using namespace openiss;

/* Helper functions */
OIGestureType convertNuiTrackGestureType( tdv::nuitrack::GestureType p_eGestureType );

/* Constructor */
OINuiTrackGestureTracker::OINuiTrackGestureTracker() = default;

/* Destructor */
OINuiTrackGestureTracker::~OINuiTrackGestureTracker() = default;

/* Initialize Tracker */
OIStatus OINuiTrackGestureTracker::init()
{
    tdv::nuitrack::Nuitrack::init();
    m_eStatus = OIStatus::STATUS_OK;
///
    m_DepthSensor = tdv::nuitrack::DepthSensor::create();
    m_gestureRecognizer = tdv::nuitrack::GestureRecognizer::create();
    m_HandTracker = tdv::nuitrack::HandTracker::create();
    m_UserTracker = tdv::nuitrack::UserTracker::create();
//
    m_DepthSensor->connectOnNewFrame(std::bind(&OINuiTrackGestureTracker::onNewDepthFrame, this, std::placeholders::_1));
    m_gestureRecognizer->connectOnNewGestures(std::bind( &OINuiTrackGestureTracker::onNewGestures, this, std::placeholders::_1 ));
    m_gestureRecognizer->connectOnUserStateChange(std::bind( &OINuiTrackGestureTracker::onUserStateChange, this, std::placeholders::_1 ));
    m_HandTracker->connectOnUpdate( std::bind(&OINuiTrackGestureTracker::onHandUpdate, this, std::placeholders::_1 ));
    m_UserTracker->connectOnUpdate( std::bind(&OINuiTrackGestureTracker::onUserUpdate, this, std::placeholders::_1) );
    m_UserTracker->connectOnNewUser( std::bind(&OINuiTrackGestureTracker::onNewUser, this, std::placeholders::_1) );
    m_UserTracker->connectOnLostUser( std::bind(&OINuiTrackGestureTracker::onLostUser, this, std::placeholders::_1) );
//
    m_DepthSensor->setMirror(false);
    m_depth_callback_counter = 0;
    m_gesture_callback_counter = 0;
    m_hand_callback_counter = 0;
    tdv::nuitrack::Nuitrack::run();
    return m_eStatus;
}

/* Update Tracker data */
OIStatus OINuiTrackGestureTracker::update()
{
    tdv::nuitrack::Nuitrack::update();
    return m_eStatus;
}

/* Stop Tracker */
OIStatus OINuiTrackGestureTracker::stop()
{
    m_gestureRecognizer->destroyGestureRecognizer();
    return m_eStatus;
}

/* Start gesture detection. */
OIStatus OINuiTrackGestureTracker::startGestureDetection()
{
    return m_eStatus;
}

/* Stop gesture detection. */
OIStatus OINuiTrackGestureTracker::stopGestureDetection()
{
    m_eStatus = stop();
    return m_eStatus;
}

/* Start Hand Tracking. */
OIStatus OINuiTrackGestureTracker::startHandTracking()
{
    return m_eStatus;
}

/* Stop Hand Tracking. */
OIStatus OINuiTrackGestureTracker::stopHandTracking()
{
    return m_eStatus;
}

/* Return available gestures. */
std::vector<OIGestureData> OINuiTrackGestureTracker::getGestures()
{
    return m_eGestureData;
}

/* Return available hands. */
std::vector<OIHandData> OINuiTrackGestureTracker::getHands()
{
    return m_eHandData;
}

/* Return most recent frame. */
OIDepthFrame* OINuiTrackGestureTracker::getDepthFrame()
{
    return &m_GFrame;
}

/* Real coordinates to Projective Coordinates*/
void OINuiTrackGestureTracker::convertHandCoordinatesToDepth(float p_x, float p_y, float p_z, float *p_OutX, float *p_OutY)
{
    m_projectiveCoordinates = m_DepthSensor->convertRealToProjCoords(-p_x, p_y, p_z); // <- p_x coordinate manually negated here!
    *p_OutX = m_projectiveCoordinates.x;
    *p_OutY = m_projectiveCoordinates.y;
}

/* Projective coordinates to Real Coordinates*/
void OINuiTrackGestureTracker::convertDepthCoordinatesToHand(int p_x, int p_y, int p_z, float *p_OutX, float *p_OutY)
{
    tdv::nuitrack::Vector3 l_projectiveCoordinates;
    l_projectiveCoordinates.x = (float) p_x;
    l_projectiveCoordinates.y = (float) p_y;
    l_projectiveCoordinates.z = (float) p_z;
    m_realCoordinates = m_DepthSensor->convertProjToRealCoords(l_projectiveCoordinates);
    *p_OutX = m_realCoordinates.x;
    *p_OutY = m_realCoordinates.y;
}

void OINuiTrackGestureTracker::onNewDepthFrame(tdv::nuitrack::DepthFrame::Ptr p_Frame)
{
    ++m_depth_callback_counter;
    m_outputMode = m_DepthSensor->getOutputMode();
    m_GFrame.setHeight(m_outputMode.yres);
    m_GFrame.setWidth(m_outputMode.xres);
    m_GFrame.setDepthData(p_Frame->getData());
//    std::cout << "depth_callback_counter [nuitrack]: " << m_depth_callback_counter << std::endl;
}

void OINuiTrackGestureTracker::onNewGestures( const tdv::nuitrack::GestureData::Ptr p_gestureData)
{
    ++m_gesture_callback_counter;
    this->m_eGestureData.clear();
    std::vector<tdv::nuitrack::Gesture> gestures = p_gestureData->getGestures();
    for (const auto &gesture : gestures)
    {
        m_eGesture.setGestureType(convertNuiTrackGestureType(gesture.type));
        this->m_eGestureData.push_back(m_eGesture);
    }
//    std::cout << "gesture_callback_counter [nuitrack]: " << m_gesture_callback_counter << std::endl;
}

void OINuiTrackGestureTracker::onHandUpdate(tdv::nuitrack::HandTrackerData::Ptr p_handData)
{
    ++m_hand_callback_counter;
    this->m_eHandData.clear();
    std::vector<tdv::nuitrack::UserHands> hands = p_handData->getUsersHands();

    for (const auto &hand :  hands)
    {
        ++m_handID;
        m_eLeftHand.setHandPosition
                (
                        hand.leftHand->xReal,
                        hand.leftHand->yReal,
                        hand.leftHand->zReal
                );
        m_eLeftHand.setHandID(m_handID);

        ++m_handID;
        m_eRightHand.setHandPosition
                (
                        hand.rightHand->xReal,
                        hand.rightHand->yReal,
                        hand.rightHand->zReal
                );
        m_eRightHand.setHandID(m_handID);
        this->m_eHandData.push_back(m_eRightHand);
        this->m_eHandData.push_back(m_eLeftHand);
    }
//    std::cout << "hand_callback_counter [nuitrack]: " << m_hand_callback_counter << std::endl;
    m_handID = 0;
}

void OINuiTrackGestureTracker::onUserUpdate(tdv::nuitrack::UserFrame::Ptr p_userFrameData)
{
    auto users = p_userFrameData->getUsers();
    for (auto user : users)
    {
        //
    }
}

/* User Module being used here to set Hand states which by default are not given by NuiTrack
 * But are desirable for extra control over application layer.*/
void OINuiTrackGestureTracker::onUserStateChange(tdv::nuitrack::UserStateData::Ptr p_userStateData)
{
    auto userStates = p_userStateData->getUserStates();
    for(auto userState : userStates)
    {
        if(userState.state == tdv::nuitrack::UserStateType::USER_ACTIVE)
        {
            m_eRightHand.setHandState(OIHandState::HAND_IS_TRACKING);
            m_eLeftHand.setHandState(OIHandState::HAND_IS_TRACKING);
        }
    }
}

void OINuiTrackGestureTracker::onNewUser(int p_userID)
{
    //std::cout << "New User in Frame!!" << std::endl;
    m_eRightHand.setHandState(OIHandState::HAND_IS_NEW);
    m_eLeftHand.setHandState(OIHandState::HAND_IS_NEW);
}

void OINuiTrackGestureTracker::onLostUser(int p_userID)
{
    m_eRightHand.setHandState(OIHandState::HAND_IS_LOST);
    m_eLeftHand.setHandState(OIHandState::HAND_IS_LOST);
    //std::cout << "Lost User in Frame!!" << std::endl;
}

/**/
OIGestureType convertNuiTrackGestureType(tdv::nuitrack::GestureType p_eGestureType)
{
    switch (p_eGestureType)
    {
        case tdv::nuitrack::GestureType::GESTURE_PUSH:
            std::cout << "NuiTrack: GESTURE_PUSH" << std::endl;
            return OIGestureType::GESTURE_PUSH;

        case tdv::nuitrack::GestureType::GESTURE_SWIPE_DOWN:
            std::cout << "NuiTrack: GESTURE_SWIPE_DOWN" << std::endl;
            return OIGestureType::GESTURE_SWIPE_DOWN;

        case tdv::nuitrack::GestureType::GESTURE_SWIPE_UP:
            std::cout << "NuiTrack: GESTURE_SWIPE_UP" << std::endl;
            return OIGestureType::GESTURE_SWIPE_UP;

        case tdv::nuitrack::GestureType::GESTURE_SWIPE_LEFT:
            std::cout << "NuiTrack: GESTURE_SWIPE_LEFT" << std::endl;
            return OIGestureType::GESTURE_SWIPE_LEFT;

        case tdv::nuitrack::GestureType::GESTURE_SWIPE_RIGHT:
            std::cout << "NuiTrack: GESTURE_SWIPE_RIGHT" << std::endl;
            return OIGestureType::GESTURE_SWIPE_RIGHT;

        case tdv::nuitrack::GestureType::GESTURE_WAVING:
            std::cout << "NuiTrack: GESTURE_WAVING" << std::endl;
            return OIGestureType::GESTURE_WAVING;
        default:
            throw std::runtime_error("No such gesture type!");
    }
}
//
// E.O.F