//
// Created by Serguei Mokhov
//

#include "OIKinectAzure.hpp"

using namespace openiss;

OIKinectAzure::OIKinectAzure()
    : OIDevice()
{

}

OIKinectAzure::~OIKinectAzure()
{
    delete mpDepthOIFrame;
    delete mpColorOIFrame;
    delete mpIrOIFrame;
}

void OIKinectAzure::init()
{
    OIStatus l_tStatus = OIStatus::STATUS_DEFAULT;

    this->m_oK4AConfig = K4A_DEVICE_CONFIG_INIT_DISABLE_ALL;

    this->m_oK4AConfig.camera_fps = K4A_FRAMES_PER_SECOND_30;
    this->m_oK4AConfig.depth_mode = K4A_DEPTH_MODE_WFOV_2X2BINNED;
    this->m_oK4AConfig.color_format = K4A_IMAGE_FORMAT_COLOR_BGRA32;
    this->m_oK4AConfig.color_resolution = K4A_COLOR_RESOLUTION_720P;

    // From SDK: This means that we'll only get captures that have both color and
    // depth images, so we don't need to check if the capture contains
    // a particular type of image.
    //
    this->m_oK4AConfig.synchronized_images_only = true;

    this->m_bInitialized = true;

    l_tStatus = OIStatus::STATUS_OK;
    this->m_eStatus = l_tStatus;
}

void OIKinectAzure::open()
{
    OIStatus l_tStatus = OIStatus::STATUS_DEFAULT;

    if(this->m_bInitialized)
    {
#ifdef OPENISS_DEBUG
        std::cout << "OIKinectAzure: Started opening K4A device..." << std::endl;
#endif
        
        this->m_oRawDevice = k4a::device::open(K4A_DEVICE_DEFAULT);
        this->m_oRawDevice.start_cameras(&this->m_oK4AConfig);

#ifdef OPENISS_DEBUG
        std::cout << "OIKinectAzure: Finished opening K4A device." << std::endl;
#endif

        l_tStatus = OIStatus::STATUS_OK;
    }
    else
    {
        std::cerr << "OIKinectAzure: K4A device not initialized. Did you call init()?" << std::endl;
        l_tStatus = OIStatus::STATUS_FAIL;
    }

    this->m_eStatus = l_tStatus;
}

OIFrame* OIKinectAzure::readFrame(openiss::StreamType p_eStreamType)
{
    if(this->m_bInitialized == false)
    {
        std::cerr << "OIKinectAzure: K4A device not initialized. Did you call init()?" << std::endl;
        return nullptr;
    }

    // From SDK: Poll the device for new image data.
    //
    // We set the timeout to 0 so we don't block if there isn't an available frame.
    //
    // If we don't have new image data, we'll just reuse the textures we generated
    // from the last time we got a capture.
    //
    k4a::capture l_oCapture;

    if(this->m_oRawDevice.get_capture(&l_oCapture, std::chrono::milliseconds(0)))
    {
        switch(p_eStreamType)
        {
            case openiss::StreamType::COLOR_STREAM:
            {
                const k4a::image l_oColorImage = l_oCapture.get_color_image();

                if(this->mpColorOIFrame == nullptr)
                {
                    this->mpColorOIFrame = new OIDataFrame
                    (
                        FrameType::COLOR_FRAME,
                        (void *)l_oColorImage.get_buffer(),
                        4, // BGRA, TODO, fix hardcoding
                        l_oColorImage.get_width_pixels(),
                        l_oColorImage.get_height_pixels()
                    );
                }
                else
                {
                    static_cast<OIDataFrame*>(this->mpColorOIFrame)->setData((void *)l_oColorImage.get_buffer());
                }

                return this->mpColorOIFrame;

                break;
            }

            case openiss::StreamType::DEPTH_STREAM:
            {
                const k4a::image l_oDepthImage = l_oCapture.get_depth_image();

                if(this->mpDepthOIFrame == nullptr)
                {
                    this->mpDepthOIFrame = new OIDataFrame
                    (
                        FrameType::COLOR_FRAME,
                        (void *)l_oDepthImage.get_buffer(),
                        2, // uint16_t, TODO, fix hardcoding
                        l_oDepthImage.get_width_pixels(),
                        l_oDepthImage.get_height_pixels()
                    );
                }
                else
                {
                    static_cast<OIDataFrame*>(this->mpDepthOIFrame)->setData((void *)l_oDepthImage.get_buffer());
                }

                return this->mpDepthOIFrame;
                break;
            }
            
            case openiss::StreamType::REGISTERED_STREAM:
                std::cerr << "OIKinectAzure: REGISTERED_STREAM not implemented yet." << std::endl;
            default:
                std::cerr << "OIKinectAzure: currently unsupported stream type: " << p_eStreamType << std::endl;
                return nullptr;
                break;
        }
    }

    return nullptr;
}

// See k4a_calibration_camera_t::intrinsics
openiss::Intrinsic OIKinectAzure::getIntrinsic(openiss::StreamType p_eStreamType)
{
    Intrinsic i;

    std::cerr << "OIKinectAzure::getIntrinsic() not implemented yet." << std::endl;

    return i;
}

openiss::Extrinsic OIKinectAzure::getExtrinsic(openiss::StreamType p_eStreamTypeFrom, openiss::StreamType p_eStreamTypeTo)
{
    Extrinsic e;

    std::cerr << "OIKinectAzure::getExtrinsic() not implemented yet." << std::endl;

    return e;
}

float OIKinectAzure::getDepthScale()
{
    std::cerr << "OIKinectAzure::getDepthScale() not implemented yet." << std::endl;
    return depthScale;
}

bool OIKinectAzure::enableRegistered()
{
    if(this->m_bInitialized == true)
    {
        this->m_oK4AConfig.synchronized_images_only = true;
        return true;
    }

    return false;
}

bool openiss::OIKinectAzure::enableDepth()
{
    std::cerr << "OIKinectAzure::enableDepth() not implemented yet." << std::endl;
    return false;
}

bool OIKinectAzure::enableColor()
{
    std::cerr << "OIKinectAzure::enableColor() not implemented yet." << std::endl;
    return false;
}

void OIKinectAzure::close()
{
   std::cerr << "OIKinectAzure::close() not implemented yet." << std::endl;
} 

void* OIKinectAzure::rawDevice()
{
    return &this->m_oRawDevice;
}

// EOF
