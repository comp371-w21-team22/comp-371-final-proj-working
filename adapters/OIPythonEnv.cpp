#include "OIPythonEnv.hpp"

#include <iostream>

openiss::OIPythonEnv::OIPythonEnv() {
    // init python interpreter
    Py_Initialize();
    if (!Py_IsInitialized()) {
        std::cout << "Python initialization failed!" << std::endl;
        exit(1);
    }

    PyRun_SimpleString("import sys");
    std::string path = PROJECT_PYTHON_ROOT;
    std::string changeDirCmd = std::string("sys.path.append(\"") + path + "\")";
    PyRun_SimpleString(changeDirCmd.c_str());
}

void openiss::OIPythonEnv::initPyWorkingPath(std::vector<std::string> paths) {
    for(auto it = paths.begin(); it != paths.end(); it++) {
        std::string addPathCommand = std::string("sys.path.append(\"") + (*it) + "\")";
        PyRun_SimpleString(addPathCommand.c_str());
    }
}

void openiss::OIPythonEnv::showWokingPath() {    
    PyRun_SimpleString("print(sys.path)");
}

openiss::OIPythonEnv::~OIPythonEnv() {
    Py_Finalize();
}

PyObject *openiss::OIPythonEnv::getPyModule(char *name) {
    if (containsModule(name)) {
        return modules[name];
    }
    return nullptr;
}

void openiss::OIPythonEnv::importPyModule(char *name) {
    PyObject *pName = PyUnicode_DecodeFSDefault(name);
    PyObject *pm = PyImport_Import(pName);
    Py_DECREF(pName);
    if (!pm) {
        std::cout << "[ERROR] Python import [" << name
                  << "] module failed." << std::endl;
        exit(1);
    }
    addModules(name, pm);
}

PyObject* openiss::OIPythonEnv::createPyInstance(char *moduleName, char *className, const char *format) {
    PyObject *pModule = getPyModule(moduleName);
    PyObject *pInstance = PyObject_CallMethod(pModule, className, format);
    Py_INCREF(pInstance);
    return pInstance;
}

PyObject* openiss::OIPythonEnv::loadPyMethod(PyObject *caller, char *funcName) {
    PyObject *pAttr = PyObject_GetAttrString(caller, funcName);
    Py_INCREF(pAttr);
    return pAttr;
}

PyObject* openiss::OIPythonEnv::invokePyMethod(PyObject *target, PyObject *args) {
    if (target && PyCallable_Check(target)) {
        PyObject* pResult = PyObject_CallObject(target, args);
        Py_INCREF(pResult);
        return pResult;
    }
    else {
        std::cout << "[error] cannot invoke: " << target << " ." << std::endl; 
        exit(-1);
    }
}

// ****************** low level methods ************************

bool openiss::OIPythonEnv::containsModule(char *name) {
    return modules.find(name) != modules.end();
}

void openiss::OIPythonEnv::addModules(char *name, PyObject *module) {
    if (!containsModule(name)) {
        PyImport_AddModule(name);
        modules[name] = module;
    }
}

void openiss::OIPythonEnv::removeModules(char *name) {
    if (containsModule(name)) {
        modules.erase(name);
    }
}

