//
// Created by Yiran Shen on 01/04/19
// 

#include "FER2D.hpp"
#include <iostream>

static PyObject* fromMatToNdArray(Mat &mat);
static Mat fromNdArrayToMat(PyObject *ndarray);

static PyObject* fromMatToNdArray(Mat &mat) {
    Mat contiMat;
    // make sure mat is continuous
    if (!mat.isContinuous()) {
        contiMat = mat.clone();
    } else {
        contiMat = mat;
    }
    npy_intp dims[] = {contiMat.rows, contiMat.cols, contiMat.channels()};
    PyObject *pNdArray = PyArray_SimpleNewFromData(contiMat.channels(), dims, NPY_UINT8, contiMat.data);
    Py_INCREF(pNdArray);
    return pNdArray;
}

static Mat fromNdArrayToMat(PyObject *ndarray) {
    PyArrayObject *arr;
    PyArray_OutputConverter(ndarray, &arr);
    npy_intp *shape = PyArray_SHAPE(arr);
    Mat mat(shape[0], shape[1], CV_32FC1, PyArray_DATA(arr));
    return mat;
}

int openiss::FER2D::init() {
    char *modName = const_cast<char *>("fer_2d");
    import_array();
    env.importPyModule(modName);
    pInstance = env.createPyInstance(modName, const_cast<char *>("FER2D"), "");
    pFuncPredict = env.loadPyMethod(pInstance, const_cast<char *>("predict"));
}

openiss::FER2D::FER2D() {
    init();
}

openiss::FER2D::~FER2D() {
    Py_DECREF(pFuncPredict);
    Py_DECREF(pInstance);
}

cv::Mat openiss::FER2D::predict(cv::Mat& image) {
    // convert imageVector to n frame image sequence gif mat

    if (image.rows == 0) {
        std::cout << "FER2D predict input must be not empty image " << std::endl;
        cv::Mat temp;
        return temp;
    }

    // opencv can not read gif (n frame image, so we can not pass 1 gif with 8 frames but 8 frames )
    pImageArg1 = fromMatToNdArray(image);

    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, pImageArg1);

    // pass to python
    pResult = env.invokePyMethod(pFuncPredict, pArgs);
    Py_DECREF(pArgs);
    Py_DECREF(pImageArg1);

    // convert the result back to C++ format
    cv::Mat resultMat = fromNdArrayToMat(pResult);
    Py_DECREF(pResult);
    return resultMat;

}

