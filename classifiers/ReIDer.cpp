//
// Created by h_lai on 28/03/19.
//

#include "ReIDer.hpp"

static PyObject* fromMatToNdArray(Mat &mat);
static Mat fromNdArrayToMat(PyObject *ndarray);

static PyObject* fromMatToNdArray(Mat &mat) {
    Mat contiMat;
    // make sure mat is continuous
    if (!mat.isContinuous()) {
        contiMat = mat.clone();
    } else {
        contiMat = mat;
    }
    npy_intp dims[] = {contiMat.rows, contiMat.cols, contiMat.channels()};
    PyObject *pNdArray = PyArray_SimpleNewFromData(contiMat.channels(), dims, NPY_UINT8, contiMat.data);
    Py_INCREF(pNdArray);
    return pNdArray;
}

static Mat fromNdArrayToMat(PyObject *ndarray) {
    PyArrayObject *arr;
    PyArray_OutputConverter(ndarray, &arr);
    npy_intp *shape = PyArray_SHAPE(arr);
    Mat mat(shape[0], shape[1], CV_8UC3, PyArray_DATA(arr));
    return mat;
}


int openiss::ReIDer::init() {
    char *modName = const_cast<char *>("re_id");
    import_array();
    env.importPyModule(modName);
    pInstance = env.createPyInstance(modName, const_cast<char *>("ReIDer"), "");
    pFuncPredict = env.loadPyMethod(pInstance, const_cast<char *>("predict"));
}

openiss::ReIDer::ReIDer() {
    init();
}

openiss::ReIDer::~ReIDer() {
    Py_DECREF(pFuncPredict);
    Py_DECREF(pInstance);
}

string openiss::ReIDer::predict(Mat &image1) {
    pImageArg1 = fromMatToNdArray(image1);

    PyObject *pArgs = PyTuple_New(1);
    PyTuple_SetItem(pArgs, 0, pImageArg1);

    // pass to python
    pResult = env.invokePyMethod(pFuncPredict, pArgs);
    if (pResult == nullptr) {
        cout << "pResult is null" << endl;
    }
    Py_DECREF(pArgs);
    Py_DECREF(pImageArg1);

    // convert the result back to C++ format
    // char* result = PyBytes_AsString(pResult);
    // if (result == nullptr) {
    //     cout << "result is null" << endl;
    // }

    char* result;
    PyArg_Parse(pResult, "s", &result);
    Py_DECREF(pResult);
    return result;
}

