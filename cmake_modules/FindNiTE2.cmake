#message("= = = = = = = = = = = = = = = = = = = = = = = = ")
#message("try to find NITE2_INCLUDE_DIR and NITE2_LIBRARY")

# for Mac, to find the nite2, need to specify the path
# if it is not in the system's default path, or set the enviroment

# for linux, the better way is the set two environment variables
# this two variables can be found inside a file after running
# the install.sh script with the name "NiTEDevEnvironment"

find_path(
        NITE2_INCLUDE_DIR NiTE.h
        /usr/local/include/nite2 /usr/include/nite2 $ENV{NITE2_INCLUDE} PATH
)
find_library(
        NITE2_LIBRARY NiTE2
        /usr/local/lib/ni2 /usr/lib/ni2 $ENV{NITE2_REDIST} $ENV{NITE2_REDIST64} PATH
)

set(NITE2_INCLUDE_DIRS ${NITE2_INCLUDE_DIR})
set(NITE2_LIBRARIES ${NITE2_LIBRARY})

if(NITE2_INCLUDE_DIR AND NITE2_LIBRARY)
#    message("NITE2_INCLUDE_DIR and NITE2_LIBRARY found")
    set(NITE2_FOUND TRUE)
    include_directories(${NITE2_INCLUDE_DIRS})

    # install the data file
    install(DIRECTORY ${PROJECT_SOURCE_DIR}/models/NiTE2/ DESTINATION ${PROJECT_BINARY_DIR}/NiTE2/)

else(NITE2_INCLUDE_DIR AND NITE2_LIBRARY)
    message("NITE2_INCLUDE_DIR and NITE2_LIBRARY not found")
endif(NITE2_INCLUDE_DIR AND NITE2_LIBRARY)
#message("= = = = = = = = = = = = = = = = = = = = = = = = ")
