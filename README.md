# To compile

- mkdir build && cd build
- cmake -L ..
- make
- make install
- copy assets folder into build/examples/webcam_test
- cd into build/examples/webcam_test
- ./webcam_test