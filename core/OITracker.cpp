#include "OITracker.hpp"

#include <iostream>

namespace openiss
{

// TODO: is clear() the right thing todo?
const std::vector<shared_ptr<OIUserData>> OITrackerFrame::getUsers()
{
    mAllUsers.clear();

    for (auto it = mAvailableUsers.begin(); it != mAvailableUsers.end(); it++)
    {
        mAllUsers.push_back(it->second);
    }

    return mAllUsers;
}

// TODO: return static 1 or 2 users
const OIUserData& OITrackerFrame::getUserById(int userId) const
{
    auto entry = mAvailableUsers.find(userId);

    if(entry != mAvailableUsers.end())
    {
        return *(entry->second);
    }

    std::cerr << "Invalid user id " << userId << std::endl;
    exit(-1);
}

const OIUserMap& OITrackerFrame::getUserMap() const
{
    return *mpOIUserMap;
}

std::vector<JointType>& OITrackerFrame::getSupportedJointType()
{
    return supportedJointType;
}

void OITrackerFrame::prepareSupportedJoints()
{
    // the order must be maintained
    supportedJointType.push_back(JointType::JOINT_HEAD);
    supportedJointType.push_back(JointType::JOINT_NECK);
    supportedJointType.push_back(JointType::JOINT_LEFT_SHOULDER);
    supportedJointType.push_back(JointType::JOINT_RIGHT_SHOULDER);
    supportedJointType.push_back(JointType::JOINT_LEFT_ELBOW);
    supportedJointType.push_back(JointType::JOINT_RIGHT_ELBOW);
    supportedJointType.push_back(JointType::JOINT_LEFT_HAND);
    supportedJointType.push_back(JointType::JOINT_RIGHT_HAND);
    supportedJointType.push_back(JointType::JOINT_TORSO);
    supportedJointType.push_back(JointType::JOINT_LEFT_HIP);
    supportedJointType.push_back(JointType::JOINT_RIGHT_HIP);
    supportedJointType.push_back(JointType::JOINT_LEFT_KNEE);
    supportedJointType.push_back(JointType::JOINT_RIGHT_KNEE);
    supportedJointType.push_back(JointType::JOINT_LEFT_FOOT);
    supportedJointType.push_back(JointType::JOINT_RIGHT_FOOT);
}


} // end of namespace
