#include "OIDevice.hpp"

namespace openiss
{

OIDevice::OIDevice()
        // TODO: revise the default status value
        : m_eStatus(OIStatus::STATUS_OK)
        , mpColorOIFrame(nullptr)
        , mpDepthOIFrame(nullptr)
        , mpIrOIFrame(nullptr)
{

}

OIDevice::~OIDevice()
{
    delete mpColorOIFrame;
    delete mpDepthOIFrame;
    delete mpIrOIFrame;

    mpColorOIFrame = nullptr;
    mpDepthOIFrame = nullptr;
    mpIrOIFrame = nullptr;
}

OIStatus OIDevice::getStatus()
{
    return this->m_eStatus;
}

bool OIDevice::enableColor()
{
    if(!m_is_enable_rgb)
    {
        m_is_enable_rgb = true;
        return true;
    }

    return false;
}

bool OIDevice::enableDepth()
{
    if(!m_is_enable_depth)
    {
        m_is_enable_depth = true;
        return true;
    }

    return false;
}

// TODO: do not mix registered and IR
bool OIDevice::enableRegistered()
{
    if(!m_is_enable_registered)
    {
        m_is_enable_registered = true;
        return true;
    }

    return false;
}

bool OIDevice::enable()
{
    enableColor();
    enableDepth();
    enableRegistered();
    return true;
}

} // namespace

// EOF
