#include "OIDevice.hpp"
#include "OIUser.hpp"
#include "OINullSkeletonTracker.hpp"
#include "OITools.hpp"

namespace openiss
{

// /////////////////////////////////////////////////////////////////////////////
// OINullSkeletonTracker class implementation
// /////////////////////////////////////////////////////////////////////////////

OINullSkeletonTracker::OINullSkeletonTracker(OIDevice& pDev)
{
    //mStatus = mpTracker->create((openni::Device*) pDev.rawDevice());
    //OIUtilities::checkStatus(mStatus, "cannot create user tracker");
    std::cout << "OpenISS null skeleton tracker instantiated..." << std::endl;
    this->m_eStatus = OIStatus::STATUS_DEFAULT;
}

OINullSkeletonTracker::~OINullSkeletonTracker()
{
    std::cout << "OpenISS null tracker destroyed..." << std::endl;
}

OIStatus OINullSkeletonTracker::init()
{
    this->m_eStatus = OIStatus::STATUS_OK;
    std::cout << "OINullSkeletonTracker initalized..." << std::endl;
    return this->m_eStatus;
}

OIStatus OINullSkeletonTracker::update()
{
    std::cout << "OINullSkeletonTracker initalized..." << std::endl;
    return this->m_eStatus;
}

OIStatus OINullSkeletonTracker::stop()
{
    std::cout << "OINullSkeletonTracker fully stopped..." << std::endl;
    return this->m_eStatus;
}

void OINullSkeletonTracker::startTracking()
{
    std::cout << "OINullSkeletonTracker::startTracking() called..." << std::endl;
}

void OINullSkeletonTracker::stopTracking()
{
    std::cout << "OINullSkeletonTracker::stopTracking() called..." << std::endl;
}

OITrackerFrame* OINullSkeletonTracker::readFrame(OITrackerFrame* p_poTrackerFrame)
{
#ifdef OPENISS_DEBUG
    std::cout << "OINullSkeletonTracker::readFrame() called..." << std::endl;
    std::cout << "  p_poTrackerFrame = " << p_poTrackerFrame << std::endl;
#endif

    OITrackerFrame* l_poFrame = p_poTrackerFrame;

    if(l_poFrame == nullptr)
    {
        OITrackerFactory l_oFactory;
        l_poFrame = l_oFactory.createTrackerFrame(nullptr).get();
    }

    return l_poFrame;
}

void OINullSkeletonTracker::mapJoint2Depth
(
    float x,
    float y,
    float z,
    float* p_pfOutX,
    float* p_pfOutY
) const
{
#ifdef OPENISS_DEBUG
    std::cout
        << "OINullSkeletonTracker::mapJoint2Depth("
        << "x = " << x << ", "
        << "y = " << y << ", "
        << "z = " << z << ", "
        << "p_pfOutX = " << p_pfOutX << ", "
        << "p_pfOutY = " << p_pfOutY
        << ")" << std::endl;
#endif 

    // TODO make sure it is properly simulated
    *p_pfOutX = x;
    *p_pfOutY = y;
}

void OINullSkeletonTracker::mapDepth2Joint
(
    int x,
    int y,
    int z,
    float* p_pfOutX,
    float* p_pfOutY
) const
{
#ifdef OPENISS_DEBUG
    std::cout
        << "OINullSkeletonTracker::mapDepth2Joint("
        << "x = " << x << ", "
        << "y = " << y << ", "
        << "z = " << z << ", "
        << "p_pfOutX = " << p_pfOutX << ", "
        << "p_pfOutY = " << p_pfOutY
        << ")" << std::endl;
#endif 

    // TODO make sure it is properly simulated
    *p_pfOutX = x;
    *p_pfOutY = y;
}

// /////////////////////////////////////////////////////////////////////////////
// OINullTrackerFrame class implementation
// /////////////////////////////////////////////////////////////////////////////

OINullTrackerFrame::OINullTrackerFrame()
{
    prepareSupportedJoints();
    update();
    std::cout << "OINullTrackerFrame called and prepared supported joints..." << std::endl;
}

OINullTrackerFrame::~OINullTrackerFrame()
{
    delete mpOIUserMap;
    std::cout << "OINullTrackerFrame destroyed..." << std::endl;
}

const std::vector<shared_ptr<OIUserData>> OINullTrackerFrame::getUsers()
{
    return OITrackerFrame::getUsers();
}

// TODO: return static 1 or 2 users
const OIUserData& OINullTrackerFrame::getUserById(int userId) const
{
    return OITrackerFrame::getUserById(userId);
}

// TODO: port to Null
//void OINullTrackerFrame::update(nite::UserTracker* tracker,
//                                         nite::UserTrackerFrameRef& frameRef)
void OINullTrackerFrame::update()
{
    // update OIUserData
    //std::cerr << "OINullTrackerFrame::update()" << std::endl;

    // Assuming user ID 0 (1 user)
    int l_iDefaultUserID = 0;
    shared_ptr<OIUserData> l_pOIUser(new OIUserData(l_iDefaultUserID));
    mAvailableUsers.insert({l_iDefaultUserID, l_pOIUser});

    shared_ptr<OIUserData> pOIUserData = mAvailableUsers[l_iDefaultUserID];
    OISkeleton* pOISkeleton = pOIUserData->getSkeleton();

    // update bounding box and center point
    Point3f* centerPoint = pOIUserData->getCenterOfMass();

    // Simulated frame dimensions
    float l_fFrameWidth = 640;
    float l_fFrameHeight = 480;

    // Simulated center of mass
    centerPoint->x = l_fFrameWidth / 2.0;
    centerPoint->y = l_fFrameHeight / 3.0;
    centerPoint->z = 0.0;

    // Simulated bounding box
    Point3f* bdBox = pOIUserData->getBoundingBox();

    bdBox[0].x = l_fFrameWidth / 3.0;
    bdBox[0].y = l_fFrameHeight - l_fFrameHeight / 4.0;
    bdBox[0].z = 0.0;

    bdBox[1].x = l_fFrameWidth - l_fFrameWidth / 3.0;
    bdBox[1].y = l_fFrameHeight / 4.0;
    bdBox[1].z = 0.0;

    // Fractions proportionate to frame dimensions for simplicity
    float l_fXf = l_fFrameWidth / 6;
    float l_fYf = l_fFrameHeight / 6;
    float l_fYFudge = l_fFrameHeight / 12;

    // For now
    //   - row and column are in OpenCV-like coordinates (top down)
    //   - x, y, and z are in OpenGL-like coordinates (bottom up)

    pOISkeleton->addJointByType
    (
        JOINT_HEAD,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf, 0,
        //     JOINT_HEAD,
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2, l_fFrameHeight - l_fYf, 0,
            JOINT_HEAD,
            l_fFrameWidth / 2, l_fYf
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_NECK,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 2, 0,
        //     JOINT_NECK,
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 2
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 2, 0,
            JOINT_NECK,
            l_fFrameWidth / 2, l_fYf * 2
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_SHOULDER,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 2, 0,
        //     JOINT_LEFT_SHOULDER,
        //     l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 2
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 2, 0,
            JOINT_LEFT_SHOULDER,
            l_fFrameWidth / 2 - l_fXf, l_fYf * 2
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_SHOULDER,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 2, 0,
        //     JOINT_RIGHT_SHOULDER,
        //     l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 2
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 2, 0,
            JOINT_RIGHT_SHOULDER,
            l_fFrameWidth / 2 + l_fXf, l_fYf * 2
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_ELBOW,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge, 0,
        //     JOINT_LEFT_ELBOW,
        //     l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge, 0,
            JOINT_LEFT_ELBOW,
            l_fFrameWidth / 2 - l_fXf * 2, l_fYf * 2 - l_fYFudge
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_ELBOW,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge, 0,
        //     JOINT_RIGHT_ELBOW,
        //     l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 2 + l_fYFudge, 0,
            JOINT_RIGHT_ELBOW,
            l_fFrameWidth / 2 + l_fXf * 2, l_fYf * 2 - l_fYFudge
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_HAND,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2, 0,
        //     JOINT_LEFT_HAND,
        //     l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2, 0,
            JOINT_LEFT_HAND,
            l_fFrameWidth / 2 - l_fXf * 3, l_fYf * 2 - l_fYFudge * 2
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_HAND,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2, 0,
        //     JOINT_RIGHT_HAND,
        //     l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 2 + l_fYFudge * 2, 0,
            JOINT_RIGHT_HAND,
            l_fFrameWidth / 2 + l_fXf * 3, l_fYf * 2 - l_fYFudge * 2
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_TORSO,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 4, 0,
        //     JOINT_TORSO,
        //     l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 4
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2, l_fFrameHeight - l_fYf * 4, 0,
            JOINT_TORSO,
            l_fFrameWidth / 2, l_fYf * 4
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_HIP,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 4, 0,
        //     JOINT_LEFT_HIP,
        //     l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 4
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf, l_fFrameHeight - l_fYf * 4, 0,
            JOINT_LEFT_HIP,
            l_fFrameWidth / 2 - l_fXf / 2, l_fYf * 4
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_HIP,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 4, 0,
        //     JOINT_RIGHT_HIP,
        //     l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 4
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf, l_fFrameHeight - l_fYf * 4, 0,
            JOINT_RIGHT_HIP,
            l_fFrameWidth / 2 + l_fXf / 2, l_fYf * 4
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_KNEE,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 5, 0,
        //     JOINT_LEFT_KNEE,
        //     l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 5
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf * 2, l_fFrameHeight - l_fYf * 5, 0,
            JOINT_LEFT_KNEE,
            l_fFrameWidth / 2 - l_fXf * 2, l_fYf * 5
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_KNEE,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 5, 0,
        //     JOINT_RIGHT_KNEE,
        //     l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 5
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf * 2, l_fFrameHeight - l_fYf * 5, 0,
            JOINT_RIGHT_KNEE,
            l_fFrameWidth / 2 + l_fXf * 2, l_fYf * 5
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_LEFT_FOOT,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 6, 0,
        //     JOINT_LEFT_FOOT,
        //     l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 6
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 - l_fXf * 3, l_fFrameHeight - l_fYf * 6, 0,
            JOINT_LEFT_FOOT,
            l_fFrameWidth / 2 - l_fXf * 3, l_fYf * 6
        ))
    );

    pOISkeleton->addJointByType
    (
        JOINT_RIGHT_FOOT,

        // shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        // (
        //     l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 6, 0,
        //     JOINT_RIGHT_FOOT,
        //     l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 6
        // ))
        shared_ptr<OISkeletonJoint>(new OISkeletonJoint
        (
            l_fFrameWidth / 2 + l_fXf * 3, l_fFrameHeight - l_fYf * 6, 0,
            JOINT_RIGHT_FOOT,
            l_fFrameWidth / 2 + l_fXf * 3, l_fYf * 6
        ))
    );

    // Always tracked
    pOISkeleton->setSkeletonState(true);

    // TODO: OIUserMap
    if(mpOIUserMap == nullptr)
    {
        mpOIUserMap = new OIUserMap(640, 480);
    }

    //std::cerr << "OINullTrackerFrame::update() DONE" << std::endl;
}

} // namespace

// EOF
