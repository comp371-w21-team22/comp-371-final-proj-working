//
//  OIFace.cpp
//  Face
//  
// OIFace is based on Opencv
//  Created by Yiran Shen on 2018-07-22.
//

#include "OIFace.hpp"

// TODO: remove coupling on OpenCV
#ifdef OPENISS_OPENCV_SUPPORT
openiss::OIFace::OIFace(const cv::Mat& img)
{
    originalImg = img;
}
#endif

openiss::OIFace::OIFace()
{
}

// std::vector<std::vector<cv::Point2f>>* openiss::OIFace::getFacialLandmarks(openiss::OIFaceAdapter& faceAdapter) {

// }

// void openiss::OIFace::getExpressions(openiss::OIFaceAdapter& faceAdapter) {

// }

// void openiss::OIFace::getFaces(openiss::OIFaceAdapter& faceAdapter) {

// }

#ifdef OPENISS_OPENCV_SUPPORT
cv::Mat& openiss::OIFace::getOriginalImg()
{
    return this->originalImg;
}
#endif
