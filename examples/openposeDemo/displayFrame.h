#pragma once
class displayFrame
{
public:
	displayFrame();
	~displayFrame();
	void display(const cv::Mat& frame, const int waitKeyValue);
	void display(const std::vector<cv::Mat>& frames, const int waitKeyValue);
private:
	const std::string mWindowName;
};

