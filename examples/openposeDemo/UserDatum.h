#ifndef OPENISS_OPENPOSE_USERDATUM_H
#define OPENISS_OPENPOSE_USERDATUM_H

class UserDatum : public op::Datum
{
public:
	UserDatum();
	~UserDatum();
	int load();
};

#endif // OPENISS_OPENPOSE_USERDATUM_H
