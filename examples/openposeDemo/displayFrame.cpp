#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <iostream>
#include "displayFrame.h"

using namespace cv;
using namespace std;

//int main_new(int argc, char** argv)
//{
//	if (argc != 2)
//	{
//		cout << " Usage: display_image ImageToLoadAndDisplay" << endl;
//		cin.get();
//		return -1;
//	}
//
//	Mat image;
//	image = imread(argv[1], IMREAD_COLOR); // Read the file
//
//	if (!image.data) // Check for invalid input
//	{
//		cout << "Could not open or find the image" << std::endl;
//		cin.get();
//		return -1;
//	}
//
//	namedWindow("Display window", WINDOW_AUTOSIZE); // Create a window for display.
//	imshow("Display window", image); // Show our image inside it.
//
//	waitKey(0); // Wait for a keystroke in the window
//
//	cin.get();
//	return 0;
//}

displayFrame::displayFrame()
{
}


displayFrame::~displayFrame()
{
}


void displayFrame::display(const cv::Mat& frame, const int waitKeyValue)
{
	try
	{
		// Security check
		if (frame.empty())
			cout << "Empty frame introduced.", __LINE__, __FUNCTION__, __FILE__;

		cv::imshow(mWindowName, frame);
		if (waitKeyValue != -1)
			cv::waitKey(waitKeyValue);
	}
	catch (const std::exception& e)
	{
		cout << e.what(), __LINE__, __FUNCTION__, __FILE__;
	}
}

void displayFrame::display(const std::vector<cv::Mat>& frames, const int waitKeyValue)
{
	try
	{
		// No frames
		if (frames.empty())
			display(cv::Mat(), waitKeyValue);
		// 1 frame
		else if (frames.size() == 1u)
			display(frames[0], waitKeyValue);
		// >= 2 frames
		else
		{
			// Prepare final cvMat
			// Concat (0)
			cv::Mat cvMat = frames[0].clone();
			// Concat (1,size()-1)
			for (auto i = 1u; i < frames.size(); i++)
				cv::hconcat(cvMat, frames[i], cvMat);
			// Display it
			display(cvMat, waitKeyValue);
		}
	}
	catch (const std::exception& e)
	{
		cout << e.what(), __LINE__, __FUNCTION__, __FILE__;
	}
}