## OpenPose Installation

[OpenPose Official Installation Manual](https://github.com/CMU-Perceptual-Computing-Lab/openpose/blob/master/doc/installation.md)

1. Clone and install [OpenPose](https://github.com/CMU-Perceptual-Computing-Lab/openpose)
```
  git clone https://github.com/CMU-Perceptual-Computing-Lab/openpose.git
```

2. Project Generation & Building

 	 ##### Windows
    * Create `build` folder in `openpose`
    * Generate VS solution with CMake/[CMake GUI](https://cmake.org/runningcmake/)
        Configuration generator: Visual Studio 14 2015 Win64
    * Build OpenPose with Visual Studio
        Switch configuration from `Debug` to `Release`

    ##### Mac
    * Create `build` folder in `openpose`
    * Generate VS solution with CMake/[CMake GUI](https://cmake.org/runningcmake/)
        Configuration generator: Unix Makefile
    * Build OpenOpose
```
  cd build/
  make -j`nproc`
```

## OpenPose demo usage
Once OpenPose is installed, copy over `3rdparty`, `include` folders inside `openposeDemo` folder.

**Perform the following if VS can't find 3rdparty include folder**:
Open Visual Studio and manually include 3rdparty include folders and libraries files

**Configuration Properties** - **C/C++** - **General**
```
$(MSBuildProjectDirectory)\include;$(MSBuildProjectDirectory)\3rdparty\windows\opencv\include;$(MSBuildProjectDirectory)\3rdparty\windows\caffe\include;$(MSBuildProjectDirectory)\3rdparty\windows\caffe\include2;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.2\include;$(MSBuildProjectDirectory)\3rdparty\windows\caffe3rdparty\include\boost-1_61;$(MSBuildProjectDirectory)\3rdparty\windows\caffe3rdparty\include;%(AdditionalIncludeDirectories)
```

**Configuration Properties** - **Linker** - **Input**
```
$(MSBuildProjectDirectory)\Release\openpose.lib;$(MSBuildProjectDirectory)\3rdparty\windows\opencv\x64\vc14\lib\opencv_world310.lib;$(MSBuildProjectDirectory)\3rdparty\windows\caffe3rdparty\lib\gflags.lib;$(MSBuildProjectDirectory)\3rdparty\windows\caffe3rdparty\lib\glog.lib;$(MSBuildProjectDirectory)\3rdparty\windows\caffe\lib\caffe.lib;$(MSBuildProjectDirectory)\3rdparty\windows\caffe\lib\caffeproto.lib;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v9.2\lib\x64\cudart_static.lib;kernel32.lib;user32.lib;gdi32.lib;winspool.lib;shell32.lib;ole32.lib;oleaut32.lib;uuid.lib;comdlg32.lib;advapi32.lib
```
