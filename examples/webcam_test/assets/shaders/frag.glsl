#version 330 core

in vec3 vertexNormal;
in vec2 vertexUV;

uniform sampler2D textureSampler;
out vec4 FragColor;

void main()
{
   //FragColor = vec4(vertexNormal, 1.0f); //TODO 2 Use the normals as fragment colors

   //vec4 textureColor = texture( textureSampler, vertexUV ) * vec4(vertexNormal, 1.0f);
   vec4 textureColor = texture( textureSampler, vertexUV );
   FragColor = textureColor;
};
