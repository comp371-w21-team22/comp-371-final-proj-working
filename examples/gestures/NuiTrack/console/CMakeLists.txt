#
AUX_SOURCE_DIRECTORY(. SOURCE_FILES)

include_directories(${PROJECT_SOURCE_DIR}/src)
include_directories(${PROJECT_SOURCE_DIR}/include)

add_executable(NuiTrackSampleConsole ${SOURCE_FILES})

target_link_libraries(NuiTrackSampleConsole openiss)

# Link common relevant libraries
include(utilities)
set(is_debug 1)

link_sample_basic_libs("NuiTrackSampleConsole")

# EOF
