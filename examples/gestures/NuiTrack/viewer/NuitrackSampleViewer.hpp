//
// Created by jash on 07/02/19.
//

#ifndef OPENISS_NUITRACK_SAMPLE_VIEWER_H
#define OPENISS_NUITRACK_SAMPLE_VIEWER_H

#include <iostream>
#include <string>
#include <map>

#if defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/gl.h>
#include <GL/glut.h>
#endif

#include "OIUtils.hpp"
#include "OIDepthFrame.hpp"
#include "OINullGestureTracker.hpp"

#ifdef OPENISS_NUITRACK_SUPPORT
#include "OINuiTrackGestureTracker.hpp"
#endif

using namespace openiss;

class NuiTrackSampleViewer final
{
 public:
  NuiTrackSampleViewer();
  ~NuiTrackSampleViewer();

  // Initialize sample: initialize NuiTrack, create all required modules,
  // register callbacks and start modules.
  void init(const std::string& config = "");

  // Update the depth map, tracking and gesture recognition data,
  // then redraw the view
  bool update();
  // Release all sample resources
  void release();

 private:
  //
  int _width, _height;

#ifdef OPENISS_NUITRACK_SUPPORT
  OIGestureTracker* m_GestureTracker = new OINuiTrackGestureTracker();
#else
  OIGestureTracker* m_GestureTracker = new OINullGestureTracker();
#endif

  OIDepthFrame m_GFrame;

  // GL data
  GLuint _textureID;
  uint8_t* _textureBuffer;
  GLfloat _textureCoords[8];
  GLfloat _vertexes[8];
  //
  bool _isInitialized;
  int square(int n);
  //
  void onNewDepthFrame(OIDepthFrame& frame);
  void initTexture(int width, int height);
  void renderTexture();
  void drawTrail(OIDepthFrame& frame);
  void drawHistory(OIGestureTracker* pHandTracker, int id, HistoryBuffer<20>* pHistory, OIDepthFrame& frame);
};

#endif // OPENISS_NUITRACK_SAMPLE_VIEWER_H
