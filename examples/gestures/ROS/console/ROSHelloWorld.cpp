// Simple Hello World program to console hello world outside of the ~/catkin_ws through roscpp.
// Author Jashanjot Singh

#include <iostream>
#include "ros/ros.h"
#include "std_msgs/String.h"

int main(int argc, char** argv)
{
	ros::init(argc, argv, "listener");
	ros::start();

	ROS_INFO_STREAM("Hello World!");

	ros::spin();
	ros::shutdown();
	
	return 0;
}
