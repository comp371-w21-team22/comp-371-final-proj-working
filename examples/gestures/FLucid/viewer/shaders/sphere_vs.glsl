#version 410

layout(location = 0) in vec3 vp;

uniform mat4 model_matrix;
uniform mat4 view_matrix;
uniform mat4 projection_matrix;
uniform vec3 use_selection_color;

out vec3 color_sel;

void main ()
{
    color_sel = use_selection_color;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vp, 1.0f);
}