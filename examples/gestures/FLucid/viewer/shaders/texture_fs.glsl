#version 410

in vec2 texture_coordinates;
uniform sampler2D i_texture;
out vec4 frag_colour;

void main () 
{
	vec4 texel = texture(i_texture, texture_coordinates);
    frag_colour = texel;
    frag_colour = vec4(1.0, 1.0, 0.0, 1.0);
};