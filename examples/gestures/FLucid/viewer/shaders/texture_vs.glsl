#version 410

layout(location = 0) in vec3 vp;
layout(location = 1) in vec2 vt;

uniform mat4 model_matrix = mat4(1.0);
uniform mat4 view_matrix;
uniform mat4 projection_matrix;

out vec2 texture_coordinates;

void main () 
{
	texture_coordinates = vt;
    gl_Position = projection_matrix * view_matrix * model_matrix * vec4(vp, 1.0f);
}