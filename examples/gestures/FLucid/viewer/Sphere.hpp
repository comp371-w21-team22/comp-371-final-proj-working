//
// Created by jash on 28/06/19.
//

#ifndef OPENISS_DIGIEVISS_SPHERE_H
#define OPENISS_DIGIEVISS_SPHERE_H

#include <string>
#include <glm/vec3.hpp>

enum class Sphere_State
{
    STATIONARY,
    SELECTED,
    ATTACHED
};

class Sphere
{
public:
    Sphere() {}

    Sphere_State state = Sphere_State::STATIONARY;
    glm::vec3 sphere_position;
    int hand_id;
    std::string data;
    int sphere_id;

};

#endif // OPENISS_DIGIEVISS_SPHERE_H
