//
// This file is an example template for OpenISS project
//

#include "OpenISS.hpp"

#include <cstring>
#include <opencv2/opencv.hpp>

using namespace openiss;

int main(int argc, char** argv)
{
    const std::string sample_root_path = PROJECT_SAMPLE_ROOT;
    std::cout << sample_root_path << std::endl;

    const char* win1Name = "color image";
    const char* win2Name = "depth image";
    cv::namedWindow(win1Name);
    cv::namedWindow(win2Name);

    OIDeviceFactory factory;

#ifdef OPENISS_REALSENSE_SUPPORT
    std::shared_ptr<OIDevice> pDevice = factory.create("rs_d435");
#else
    std::shared_ptr<OIDevice> pDevice = factory.create("null");
#endif

    pDevice->open();
    pDevice->enable();

    bool shutdown = false;
    int cc = 1;

    while(!shutdown)
    {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);
        OIFrame* depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        colorFrame->show(win1Name);
        depthFrame->show(win2Name);

        // shutdown on esc
        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));

        // save image
        switch (key & 0xFF)
        {
            case 's':
                std::cout
                    << sample_root_path + "rs435/saved_images/"
                    << "color" + std::to_string(cc)
                    << std::endl;
                    
                colorFrame->save
                (
                    sample_root_path + "rs435/saved_images/",
                    "color" + std::to_string(cc++)
                );

                break;

            default:
                break;
        }
    }

    return 0;
}

// EOF
