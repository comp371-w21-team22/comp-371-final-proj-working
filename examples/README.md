
# OpenISS SDK Examples

```
_example      -- sample template starter project
ogl           -- OpenGL tutorials being augmented with OpenISS (see its own README)
calibration   -- checkerboard callibration
face          -- facial and emotion tracking/detection
gestures      -- gesture tracking over Nuitrack, NITE2 and ROS
kinect        -- OpenNI2 frame and skeleton tracking for Kinect or OpenNI2+NiTE2 devices in general
libfreenect   -- examples being ported from libfreenect API
openposeDemo  -- OpenPose example
reid          -- re-identification example (ML, TensorFlow, Keras, cross-language Python module)
rs435         -- RealSense examples
yolo          -- YOLO object detection example (ML + Python wrapper)
Processing    -- OpenISS Processing.org wrapper, relies on the `openiss-swig` project
structure     -- Structure examples
k4a           -- Kinect for Azure examples
```

## Gesture Tracking and ROS

The `gestures/ROS` directory contains simple apps like hello_world to get started with ROS from an external application point of view via linking to the required **roscpp** and other packages via **CMake**.

**NOTE**: Make sure **roscore** is running before you run any of the executables.