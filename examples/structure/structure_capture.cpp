//
// This file is an example template for OpenISS project
//

#include "OpenISS.hpp"

#include <cstring>
#include <iostream>

#ifdef OPENISS_OPENCV_SUPPORT
#include <opencv2/opencv.hpp>
#endif

using namespace openiss;

std::string sample_root_path;
void displayColorAndDepth();

int main(int argc, char** argv)
{
    sample_root_path = PROJECT_SAMPLE_ROOT;
    displayColorAndDepth();
    return 0;
}

void displayColorAndDepth() {
    const std::string sample_root_path = PROJECT_SAMPLE_ROOT;
    std::cout << sample_root_path << std::endl;

    const char* win1Name = "OpenISS color image";
    const char* win2Name = "OpenISS depth image";
    const char* win3Name = "OpenISS infrared image";

#ifdef OPENISS_OPENCV_SUPPORT
    std::cout << "Creating OpenCV windows..." << std::endl;

    cv::namedWindow(win1Name);
    cv::namedWindow(win2Name);
    cv::namedWindow(win3Name);
#endif

    std::cout << "OpenISS starting factory..." << std::endl;

    OIDeviceFactory factory;
    
    std::cout << "Creating an OpenISS device..." << std::endl;

#ifdef OPENISS_STRUCTURE_SUPPORT
    std::shared_ptr<OIDevice> pDevice = factory.create("structure");
#else
    std::shared_ptr<OIDevice> pDevice = factory.create("null");
#endif

    std::cout << "OpenISS device created" << std::endl;

    pDevice->open();
    pDevice->enable();

    std::cout << "OpenISS device enabled" << std::endl;

    int cc = 1;
    int dc = 1;
    bool shutdown = false;

    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);
        OIFrame* depthFrame = pDevice->readFrame(openiss::DEPTH_STREAM);
        OIFrame* irFrame = pDevice->readFrame(openiss::IR_STREAM);

#ifdef OPENISS_DEBUG
        std::cout
            << "OpenISS read frames: "
            << "color frame at " << colorFrame << ", "
            << "depth frame at " << depthFrame << ", "
            << "IR frame at " << irFrame
            << std::endl;
#endif

#ifdef OPENISS_OPENCV_SUPPORT
        colorFrame->show(win1Name);
        depthFrame->show(win2Name);
        irFrame->show(win3Name);

#ifdef OPENISS_DEBUG
        std::cout << "OpenISS shown frames" << std::endl;
#endif

#endif

#ifdef OPENISS_OPENCV_SUPPORT
        int key = cv::waitKey(1);
        
        // shutdown on esc
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27));
        
        // save image
        switch(key & 0xFF)
        {
                    
            case 's':
                colorFrame->save
                (
                    sample_root_path + "structure/saved_images/",
                    "color" + std::to_string(cc++)
                );

                irFrame->save
                (
                    sample_root_path + "structure/saved_images/",
                    "ir-depth" + std::to_string(dc++)
                );

                break;

            default:
                break;
        }
#endif
    }

    std::cout << "Exiting succesfully" << std::endl;
}

//EOF