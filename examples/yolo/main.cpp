
#include "OpenISS.hpp"
#include "OIDataFrame.hpp"
#include "OIFrameCVImpl.hpp"
#include "OIYoloDetector.hpp"

#include <opencv/cv.hpp>

using namespace openiss;

void detect_image() {
    OIYoloDetector detector;

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("rs_d435");
    pDevice->open();
    pDevice->enable();

    cv::Mat result;
    cv::Mat img;
    bool shutdown = false;
    while (!shutdown) {
        OIDataFrame* colorFrame = dynamic_cast<OIDataFrame *>(pDevice->readFrame(openiss::COLOR_STREAM));
        OIFrameCVImpl* cvFrame = dynamic_cast<OIFrameCVImpl *>(colorFrame->getFrame());
        img = cvFrame->getCvMat();
        result = detector.detect_image(img);
        imshow("image", result);

        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27)); // shutdown on esc
    }
}

void detect_bbox() {
    OIYoloDetector detector;
    cv::Mat img = cv::imread("/home/h_lai/Documents/testing/sample/yolo/test_image.jpg");
    vector<vector<openiss::Point2i>> bbox;
    vector<float> scores;
    detector.detect_bbox(img, bbox, scores);

    int offsety = bbox[0][1].y - bbox[0][0].y;
    int offsetx =  bbox[0][1].x -  bbox[0][0].x;
    Rect roi(bbox[0][0].y, bbox[0][0].x, offsety, offsetx);
    Mat person = img(roi);
    imshow("person", person);
    waitKey();

    // for (int i = 0; i < bbox.size(); i++) {
    //     for (int j = 0; j < 2; ++j) {
    //         std::cout << "(" << bbox[i][j].x << ", " << bbox[i][j].y << ")" << std::endl;
    //     }
    //     std::cout << scores[i] << std::endl;
    // }
}

int main() {
    detect_image();
    // detect_bbox();
    return 0;
}
