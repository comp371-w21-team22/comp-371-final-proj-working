//
// Created by Haotao Lai on 2018-08-08.
//

#include <opencv/cv.hpp>

#include "OpenISS.hpp"

using namespace openiss;

void displayColorAndDepth();

int main() {
    displayColorAndDepth();
    return 0;
}

/**
 * The following method shows how to use OpenISS API
 * to get RGB and depth image data
 */
void displayColorAndDepth() {
    const char* win1Name = "Single Face Landmarks";

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("kinect");
    pDevice->open();
    pDevice->enableColor();

    OIFaceAdapterFactory adapterFactory;
    std::shared_ptr<OIFaceAdapter> faceAdapter = adapterFactory.createAdapter("openface", pDevice.get());

    bool shutdown = false;
    while (!shutdown) {
        OIFrame* colorFrame = pDevice->readFrame(openiss::COLOR_STREAM);

        cv::Mat img = colorFrame->getOpencvMat();

        faceAdapter->drawSingleFaceOnImg(img);

    }
}
