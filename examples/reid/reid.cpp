
#include "OpenISS.hpp"

#include "OIDataFrame.hpp"
#include "OIFrameCVImpl.hpp"
#include "ReIDer.hpp"
#include "OIYoloDetector.hpp"

#include <opencv2/opencv.hpp>

#include <iostream>

using namespace openiss;

const string db_path = PROJECT_SAMPLE_ROOT;

void print_result(int result) {
    if (result) {
        cout << "same person" << endl;
    }
    else {
        cout << "different person" << endl;
    }
}


void compare_image() {
    ReIDer classifier;
    cv::Mat img1 = cv::imread(db_path + "reid/test/same/0583_c3s2_012512_02.jpg");
    string is_same_person = classifier.predict(img1);
    cout << is_same_person << endl;
    cout << "**************************" << endl;
}


void demo() {
    OIYoloDetector detector;
    ReIDer classifier;

    OIDevFactory factory;
    std::shared_ptr<OIDevice> pDevice = factory.create("rs_d435");
    pDevice->open();
    pDevice->enable();


    cv::Mat img;
    bool shutdown = false;
    while (!shutdown) {
        vector<vector<openiss::Point2i>> bbox;
        vector<float> scores;

        OIDataFrame* colorFrame = dynamic_cast<OIDataFrame *>(pDevice->readFrame(openiss::COLOR_STREAM));
        OIFrameCVImpl* cvFrame = dynamic_cast<OIFrameCVImpl *>(colorFrame->getFrame());
        img = cvFrame->getCvMat();

        detector.detect_bbox(img, bbox, scores);
        Mat person;

        // loop over all the detected people, compare them with the database
        for (int i = 0; i < bbox.size(); i++) {
            int y = bbox[i][0].y;
            int x = bbox[i][0].x;
            int offsety = bbox[i][1].y - bbox[i][0].y;
            int offsetx = bbox[i][1].x - bbox[i][0].x;

            if (y <= 0 || x <= 0 || offsetx <= 0 || offsety <= 0 || y + offsety >= img.rows || x + offsetx >= img.cols)
                continue;

//            std::cout << y << ", " << x << ", " << offsety << ", " << offsetx << ", "
//                      << y + offsety << ", " << x + offsetx << std::endl;

            Rect roi(y, x, offsety, offsetx);
            img(roi).copyTo(person);


            string whom = classifier.predict(person);

            if (whom != "none") {
                cv::rectangle(img, roi, Scalar(0, 255, 0));
                std::cout << whom << std::endl;
            }

        }
        imshow("image", img);

        int key = cv::waitKey(1);
        shutdown = shutdown || (key > 0 && ((key & 0xFF) == 27)); // shutdown on esc
    }
}



int main() {
    // compare_image();
    demo();
    return 0;
}
