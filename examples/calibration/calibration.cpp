#include <opencv2/opencv.hpp>

#include <iostream>
#include <cctype>
#include <stdio.h>
#include <string.h>


#define SQUARE_SIZE 20.0f
#define BOARD_WIDTH 7
#define BOARD_HEIGHT 9
#define SHOW_DETECTED false
#define SHOW_UNDISTORTED true
#define DURATION 0


using namespace cv;
using namespace std;

static double computeReprojectionErrors(
        const vector<vector<Point3f> > &objectPoints,
        const vector<vector<Point2f> > &imagePoints,
        const vector<Mat> &rvecs, const vector<Mat> &tvecs,
        const Mat &cameraMatrix, const Mat &distCoeffs,
        vector<float> &perViewErrors
) {
    vector<Point2f> imagePoints2;
    int i, totalPoints = 0;
    double totalErr = 0, err;
    perViewErrors.resize(objectPoints.size());

    for (i = 0; i < (int) objectPoints.size(); i++) {
        projectPoints(Mat(objectPoints[i]), rvecs[i], tvecs[i],
                      cameraMatrix, distCoeffs, imagePoints2);
        err = norm(Mat(imagePoints[i]), Mat(imagePoints2), NORM_L2);
        int n = (int) objectPoints[i].size();
        perViewErrors[i] = (float) std::sqrt(err * err / n);
        totalErr += err * err;
        totalPoints += n;
    }

    return std::sqrt(totalErr / totalPoints);
}

static void calcChessboardCorners(Size boardSize,
                                  float squareSize,
                                  vector<Point3f> &corners
) {
    corners.resize(0);
    for (int i = 0; i < boardSize.height; i++) {
        for (int j = 0; j < boardSize.width; j++) {
            corners.emplace_back(j * squareSize, i * squareSize, 0);
        }
    }
}

static bool runCalibration(
        vector<vector<Point2f>> &imagePoints, Size &imageSize,
        Size &boardSize, float squareSize,
        Mat &cameraMatrix, Mat &distCoeffs,
        vector<Mat> &rvecs, vector<Mat> &tvecs,
        vector<float> &reprojErrs, double &totalAvgErr
) {
    cameraMatrix = Mat::eye(3, 3, CV_64F);
    distCoeffs = Mat::zeros(8, 1, CV_64F);

    vector<vector<Point3f>> objectPoints(1);
    calcChessboardCorners(boardSize, squareSize, objectPoints[0]);
    objectPoints.resize(imagePoints.size(), objectPoints[0]);

    calibrateCamera(objectPoints, imagePoints, imageSize,
                    cameraMatrix, distCoeffs, rvecs, tvecs);

    bool ok = checkRange(cameraMatrix) && checkRange(distCoeffs);
    totalAvgErr = computeReprojectionErrors(objectPoints, imagePoints,
                                            rvecs, tvecs, cameraMatrix, distCoeffs, reprojErrs);

    return ok;
}


static void saveCameraParams(
        const string &filename,
        Size imageSize, Size boardSize,
        float squareSize, float aspectRatio, int flags,
        const Mat &cameraMatrix, const Mat &distCoeffs,
        const vector<Mat> &rvecs, const vector<Mat> &tvecs,
        const vector<float> &reprojErrs,
        const vector<vector<Point2f> > &imagePoints,
        double totalAvgErr
) {
    FileStorage fs(filename, FileStorage::WRITE);

    time_t tt;
    time(&tt);
    struct tm *t2 = localtime(&tt);
    char buf[1024];
    strftime(buf, sizeof(buf) - 1, "%c", t2);

    fs << "calibration_time" << buf;

    if (!rvecs.empty() || !reprojErrs.empty()) {
        fs << "nframes" << (int) std::max(rvecs.size(), reprojErrs.size());
    }
    fs << "image_width" << imageSize.width;
    fs << "image_height" << imageSize.height;
    fs << "board_width" << boardSize.width;
    fs << "board_height" << boardSize.height;
    fs << "square_size" << squareSize;

    if (flags & CALIB_FIX_ASPECT_RATIO) {
        fs << "aspectRatio" << aspectRatio;
    }

    if (flags != 0) {
        sprintf(buf, "flags: %s%s%s%s",
                flags & CALIB_USE_INTRINSIC_GUESS ? "+use_intrinsic_guess" : "",
                flags & CALIB_FIX_ASPECT_RATIO ? "+fix_aspectRatio" : "",
                flags & CALIB_FIX_PRINCIPAL_POINT ? "+fix_principal_point" : "",
                flags & CALIB_ZERO_TANGENT_DIST ? "+zero_tangent_dist" : "");
        //cvWriteComment( *fs, buf, 0 );
    }

    fs << "flags" << flags;

    fs << "camera_matrix" << cameraMatrix;
    fs << "distortion_coefficients" << distCoeffs;

    fs << "avg_reprojection_error" << totalAvgErr;
    if (!reprojErrs.empty()) {
        fs << "per_view_reprojection_errors" << Mat(reprojErrs);
    }

    if (!rvecs.empty() && !tvecs.empty()) {
        CV_Assert(rvecs[0].type() == tvecs[0].type());
        Mat bigmat((int) rvecs.size(), 6, rvecs[0].type());
        for (int i = 0; i < (int) rvecs.size(); i++) {
            Mat r = bigmat(Range(i, i + 1), Range(0, 3));
            Mat t = bigmat(Range(i, i + 1), Range(3, 6));

            CV_Assert(rvecs[i].rows == 3 && rvecs[i].cols == 1);
            CV_Assert(tvecs[i].rows == 3 && tvecs[i].cols == 1);
            //*.t() is MatExpr (not Mat) so we can use assignment operator
            r = rvecs[i].t();
            t = tvecs[i].t();
        }
        //cvWriteComment( *fs, "a set of 6-tuples (rotation vector + translation vector) for each view", 0 );
        fs << "extrinsic_parameters" << bigmat;
    }

    if (!imagePoints.empty()) {
        Mat imagePtMat((int) imagePoints.size(), (int) imagePoints[0].size(), CV_32FC2);
        for (int i = 0; i < (int) imagePoints.size(); i++) {
            Mat r = imagePtMat.row(i).reshape(2, imagePtMat.cols);
            Mat imgpti(imagePoints[i]);
            imgpti.copyTo(r);
        }
        fs << "image_points" << imagePtMat;
    }
}

static bool runAndSave(
        const string &outputFilename,
        vector<vector<Point2f>> &imagePoints,
        Size &imageSize, Size &boardSize, float squareSize,
        Mat &cameraMatrix, Mat &distCoeffs, bool writeExtrinsics, bool writePoints
) {
    vector<Mat> rvecs, tvecs;
    vector<float> reprojErrs;
    double totalAvgErr = 0;

    bool ok = runCalibration(imagePoints, imageSize, boardSize, squareSize,
                             cameraMatrix, distCoeffs, rvecs, tvecs,
                             reprojErrs, totalAvgErr);
//    if (ok) {
//        saveCameraParams(outputFilename, imageSize,
//                         boardSize, squareSize, aspectRatio,
//                         flags, cameraMatrix, distCoeffs,
//                         writeExtrinsics ? rvecs : vector<Mat>(),
//                         writeExtrinsics ? tvecs : vector<Mat>(),
//                         writeExtrinsics ? reprojErrs : vector<float>(),
//                         writePoints ? imagePoints : vector<vector<Point2f> >(),
//                         totalAvgErr);
//    }
    return ok;
}

static void prepareImageNameList(vector<string> &imagePathList) {
    string prefix = "./calibration/";
    string surfix = ".jpg";
//    string tmp = "img_depth";
    string tmp = "img_color";

    for (int i = 1; i <= 20; ++i) {
        imagePathList.push_back(prefix + tmp + std::to_string(i) + surfix);
    }
}


int main(int argc, char **argv) {
    string outputFilename;
    bool writeExtrinsics, writePoints;

    float squareSize = SQUARE_SIZE;
    Size boardSize(BOARD_WIDTH, BOARD_HEIGHT);
    Size imageSize;
    Mat cameraMatrix, distCoeffs;
    vector<vector<Point2f>> imagePoints;
    vector<string> imagePathList;

    prepareImageNameList(imagePathList);

    for (auto const &path : imagePathList) {
        Mat curImg, curImgGray;
        curImg = imread(path, CV_LOAD_IMAGE_COLOR);
        if (curImg.empty()) {
            cerr << "cannot open file: " << path << endl;
        }
        imageSize = curImg.size();
        cvtColor(curImg, curImgGray, COLOR_BGR2GRAY);

        vector<Point2f> pointbuf;
        bool found;
        found = findChessboardCorners(curImg, boardSize, pointbuf,
                                      CALIB_CB_ADAPTIVE_THRESH
                                      | CALIB_CB_FAST_CHECK
                                      | CALIB_CB_NORMALIZE_IMAGE);
        if (found) {
            cornerSubPix(curImgGray, pointbuf, Size(11, 11), Size(-1, -1),
                         TermCriteria(TermCriteria::EPS + TermCriteria::COUNT, 30, 0.1));
            drawChessboardCorners(curImg, boardSize, Mat(pointbuf), found);
            imagePoints.push_back(pointbuf);

            if (SHOW_DETECTED) {
                imshow("detected corners", curImg);
                waitKey(DURATION);
            }
        }
        else {
            cout << "cannot find chessboard in the image: " << path << endl;
        }
    }

    runAndSave(outputFilename, imagePoints, imageSize,
               boardSize, squareSize,
               cameraMatrix, distCoeffs,
               writeExtrinsics, writePoints);

    Rect roi;
    Mat newCameraMatrix = getOptimalNewCameraMatrix(cameraMatrix, distCoeffs, imageSize, 1, imageSize, &roi);
    cout << "Camera Matrix" << endl;
    cout << cameraMatrix << endl;
    cout << "Optimal Camera Matrix" << endl;
    cout << newCameraMatrix << endl;


    Mat map1, map2;
    initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),
                            newCameraMatrix,
                            imageSize, CV_16SC2, map1, map2);

//    initUndistortRectifyMap(cameraMatrix, distCoeffs, Mat(),
//                            cameraMatrix,
//                            imageSize, CV_16SC2, map1, map2);

    for (auto const &path : imagePathList) {
        Mat originalImg, fixedImg;
        originalImg = imread(path, CV_LOAD_IMAGE_COLOR);
        if (originalImg.empty()) {
            cerr << "cannot open file: " << path << endl;
        }
        remap(originalImg, fixedImg, map1, map2, INTER_LINEAR);
        if (SHOW_UNDISTORTED) {
            imshow("original image", originalImg);
            imshow("undistorted image", fixedImg(roi));
            waitKey(DURATION);
        }
    }

    return 0;
}
