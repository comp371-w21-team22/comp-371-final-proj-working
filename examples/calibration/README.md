# OpenISS Calibration examples

These are calibration examples added or modified by [Eric Lai](https://github.com/laihaotao)
during [his master's thesis](https://spectrum.library.concordia.ca/985788/)).


- `calibration.cpp` is the main example
- `calib.cpp` is the auxilary example

**The examples depend on OpenCV.** One is fully written by Eric
and the other is based on OpenCV's.

The main purpose is to use the checker-board-based
images to get the camera intrinsic and extrinsic
parameters and then save them and use them in
in the corresponding implementations/wrappers.
The 20 images currently need to be prepared in advance
in a `./calibration/` subdirectory, e.g., using
`kinect` or `rs435` color frame capture examples
and saving them.

The calibration information (two matrices) will show up in the console.
If you need to use the calibrated information currently there is no way
to use it automatically directly (you need to add logic like a mapping
from a device type to specific matrices).
The following two methods of `OIDevice` would need to be overridden
to get the intrinsic and extrinsic matrices in the implementations:

- `OIDevice::getIntrinsic()`
- `OIDevice::getExtrinsic()`

## TODO / Limitations

Currently the example is static and not using OpenISS features.
Need to make sure it can use image loaders, movie loaders or
live camera data from `OIDevice`-based sources as a command-line option.

Need to aggregate the two examples into a single application and use command-line options as needed to call different functionality.

## References

- [Haotao Lai](https://github.com/laihaotao), [*An OpenISS Framework Specialization for Deep Learning-based Person Re-identification*](https://spectrum.library.concordia.ca/985788/), Master's thesis, August 2019, Concordia University, Montreal, Canada
