# How to compile from scratch

1. clone OpenISS repo from bitbucket
2. switch to "ogl" branch and make another branch
3. copy "src/api/cpp" dir and move it to where you want your project to be
3. take all the your dependencies from PA2 (ie. include/ and cmake_modules/ ) and import them here
4. copy "examples/_example" and replace the .cpp with your own
5. set CMake_list.txt (in main dir) -> option(BUILD_EXAMPLES "Build all the samples" ON)
6. add your own sub dir in CMake_list.txt (in examples dir) "add_subdirectory(_opencv_test)"
7. copy from the "examples/_example/CMake_list.txt" and add the old stuff from the old CMake_list.txt of PA2
