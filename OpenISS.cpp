#include "OpenISS.hpp"

#include <iostream>


// The main namespace and API entry point
namespace openiss
{

OpenISS::OpenISS()
    :OpenISS(NULL_DEVICE, true, true, true, true, true, true, true)
{
}

OpenISS::OpenISS(e_sensor_adapter p_eAdapterType)
    :OpenISS(p_eAdapterType, false, false, false, false, false, false, false)
{
}

OpenISS::~OpenISS()
{
    // Free all the devices and trackers
#ifdef OPENISS_DEBUG
    std::cerr << "~OpenISS(): Clearing devices..." << std::endl;
#endif
    this->m_oSensorAdapter = nullptr;
    this->m_paoDevices.clear();

#ifdef OPENISS_DEBUG
    std::cerr << "~OpenISS(): Clearing trackers..." << std::endl;
#endif
    this->m_paoTrackers.clear();

#ifdef OPENISS_DEBUG
    std::cerr << "~OpenISS(): Shutting down..." << std::endl;
#endif
}

OpenISS::OpenISS
(
    e_sensor_adapter p_eAdapterType,
    bool p_bEnableDepth,
    bool p_bEnableRGB,
    bool p_bEnableIR,
    bool p_bEnableSkeletons,
    bool p_bEnableGestures,
    bool p_bEnableHands,
    bool p_bEnableFaces
)
{
    this->m_bDepthEnabled = p_bEnableDepth;
    this->m_bRGBEnabled = p_bEnableRGB;
    this->m_bIREnabled = p_bEnableIR;

    this->m_bSkeletonTrackingEnabled = p_bEnableSkeletons;
    this->m_bFaceTrackingEnabled = p_bEnableFaces;
    this->m_bHandTrackingEnabled = p_bEnableHands;
    this->m_bGestureTrackingEnabled = p_bEnableGestures;

    switch(p_eAdapterType)
    {
        case ADAPTER_OPENNI2:
            m_oSensorAdapter = this->m_oDeviceFactory.create("kinect");
            break;

        case ADAPTER_REALSENSE_SDK2:
            m_oSensorAdapter = this->m_oDeviceFactory.create("rs_d435");
            break;

        case ADAPTER_STRUCTURE_SDK:
            m_oSensorAdapter = this->m_oDeviceFactory.create("structure");
            break;

        case ADAPTER_AZURE_KINECT_SDK:
            m_oSensorAdapter = this->m_oDeviceFactory.create("k4a");
            break;

        case ADAPTER_RGB_CAMERA:
            std::cerr
                << "OpenISS: the explicit webcam adapter is not implemented yet." << std::endl
                << "  Use the the color stream from any supported depth sensors."
                << std::endl;
            break;

        case ADAPTER_LIBFREENECT:
            std::cerr
                << "OpenISS: the explicit libfreenect adapter is not implemented yet." << std::endl
                << "  Use the OpenNI2-based OIKinect for now instead with libfreenect."
                << std::endl;
            break;

        case ADAPTER_LIBFREENECT2:
        case ADAPTER_NUITRACK:
        case ADAPTER_ROS:
    
        case NULL_DEVICE:
        default:
        {
            m_oSensorAdapter = this->m_oDeviceFactory.create("null");

            std::cout
                << "OpenISS: created a null device for adapter type "
                << p_eAdapterType
                << std::endl;

            if(p_eAdapterType != NULL_DEVICE)
            {
                std::cerr
                    << "  NOTE: the null device ("
                    << NULL_DEVICE
                    << ") is created in place of the intended adapter as it appears we don't have it implemented."
                    << std::endl;
            }

            break;
        }
    }

	if(m_oSensorAdapter != nullptr)
	{
		m_paoDevices.push_back(m_oSensorAdapter);
	}
}

OIStatus OpenISS::init()
{
#ifdef OPENISS_DEBUG
    std::cerr << "OpenISS::init() currently always returns STATUS_OK" << std::endl;
#endif

    return OIStatus::STATUS_OK;
}

int OpenISS::enumerateDevices()
{
#ifdef OPENISS_DEBUG
    std::cerr << "OpenISS::enumerateDevices() currently partially implemented" << std::endl;
#endif

    // TODO:
    // this->m_oSensorAdapter->enumerateDevices();
    // Loop through the enumerable devices for each device type
    // and add them to the list

    return getDeviceCount();
}

int OpenISS::getDeviceCount()
{
    return this->m_paoDevices.size();
}

std::shared_ptr<OIDevice> OpenISS::getCurrentDevice()
{
    return this->m_oSensorAdapter;
}

std::shared_ptr<OIDevice> OpenISS::getDevice(int p_iDeviceNum)
{
    if(p_iDeviceNum > getDeviceCount() - 1 || p_iDeviceNum < 0)
    {
        std::cerr
            << "OpenISS::getDevice() -- device number "
            << p_iDeviceNum
            << " is out of range [0 - " << (getDeviceCount() - 1) << "]"
            << std::endl;

        return nullptr;
    }

    return this->m_paoDevices[p_iDeviceNum];
}

std::vector<std::shared_ptr<OIDevice>> OpenISS::getAllDevices()
{
    return this->m_paoDevices;
}

OIFrame* OpenISS::getDepthFrame(OIDevice* p_poDevice)
{
    std::shared_ptr<OIDevice> l_poDevice = m_oSensorAdapter;
    
    if(p_poDevice != nullptr)
    {
        l_poDevice.reset(p_poDevice);
    }

    return l_poDevice.get()->readFrame(DEPTH_STREAM);
}

OIFrame* OpenISS::getRGBFrame(OIDevice* p_poDevice)
{
    std::shared_ptr<OIDevice> l_poDevice = m_oSensorAdapter;
    
    if(p_poDevice != nullptr)
    {
        l_poDevice.reset(p_poDevice);
    }

    return l_poDevice.get()->readFrame(COLOR_STREAM);
}

OIFrame* OpenISS::getIRFrame(OIDevice* p_poDevice)
{
    std::shared_ptr<OIDevice> l_poDevice = m_oSensorAdapter;
    
    if(p_poDevice != nullptr)
    {
        l_poDevice.reset(p_poDevice);
    }

    return l_poDevice.get()->readFrame(IR_STREAM);
}

OIFrame* OpenISS::getGreenScreenedFrame(OIDevice* p_poDevice)
{
    std::cerr
        << "OpenISS::getGreenScreenedFrame(): not implemented yet..."
        << std::endl;

    return nullptr;
}

const OIUserMap* OpenISS::getUserMap(OITracker* p_poTracker)
{
    const OIUserMap* l_poUserMap = nullptr;
    OISkeletonTracker* l_poSkeletonTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bSkeletonTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getUserMap(): skeleton tracking is not enabled..."
            << std::endl;

        return l_poUserMap;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getUserMap(): no suitable trackers found..."
            << std::endl;

        return l_poUserMap;
    }

    l_poSkeletonTracker = dynamic_cast<OISkeletonTracker*>(l_poTracker);

    if(l_poSkeletonTracker != nullptr)
    {
        OITrackerFrame* l_poFrame = l_poSkeletonTracker->readFrame();
        l_poUserMap = (&l_poFrame->getUserMap());
    }
    else
    {
        std::cerr
            << "OpenISS::getUserMap(): the passed tracker is not a skeleton tracker..."
            << std::endl;
    }

    return l_poUserMap;
}

std::vector<OIUserData> OpenISS::getUsers(OITracker* p_poTracker)
{
    std::vector<OIUserData> l_paoUsers;
    OISkeletonTracker* l_poSkeletonTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bSkeletonTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getUsers(): skeleton tracking is not enabled..."
            << std::endl;

        return l_paoUsers;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getUsers(): no suitable trackers found..."
            << std::endl;

        return l_paoUsers;
    }

    l_poSkeletonTracker = dynamic_cast<OISkeletonTracker*>(l_poTracker);

    if(l_poSkeletonTracker != nullptr)
    {
        OITrackerFrame* l_poFrame = l_poSkeletonTracker->readFrame();

        std::vector<std::shared_ptr<OIUserData>> l_aoUsers = l_poFrame->getUsers();
        
        for(const auto &l_oUser : l_aoUsers)
        {
            l_paoUsers.push_back(*l_oUser);
        }
    }
    else
    {
        std::cerr
            << "OpenISS::getUsers(): the passed tracker is not a skeleton tracker..."
            << std::endl;
    }

#ifdef OPENISS_DEBUG
        std::cerr
            << "OpenISS::getUsers(): user count = "
            << l_paoUsers.size()
            << std::endl;
#endif

    return l_paoUsers;
}

OITrackerFrame* OpenISS::getUserFrame(OITracker* p_poTracker)
{
    OITrackerFrame* l_poFrame = nullptr;
    OISkeletonTracker* l_poSkeletonTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bSkeletonTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getUserFrame(): skeleton tracking is not enabled..."
            << std::endl;

        return l_poFrame;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getSkeletons(): no suitable trackers found..."
            << std::endl;

        return l_poFrame;
    }

    l_poSkeletonTracker = dynamic_cast<OISkeletonTracker*>(l_poTracker);

    if(l_poSkeletonTracker != nullptr)
    {
        l_poFrame = l_poSkeletonTracker->readFrame();
    }
    else
    {
        std::cerr
            << "OpenISS::getUserFrame(): the passed tracker is not a skeleton tracker..."
            << std::endl;
    }

    return l_poFrame;
}

std::vector<OISkeleton> OpenISS::getSkeletons(OITracker* p_poTracker)
{
    std::vector<OISkeleton> l_paoSkeletons;
    OISkeletonTracker* l_poSkeletonTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bSkeletonTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getSkeletons(): skeleton tracking is not enabled..."
            << std::endl;

        return l_paoSkeletons;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getSkeletons(): no suitable trackers found..."
            << std::endl;

        return l_paoSkeletons;
    }

    l_poSkeletonTracker = dynamic_cast<OISkeletonTracker*>(l_poTracker);

    if(l_poSkeletonTracker != nullptr)
    {
        OITrackerFrame* l_poFrame = l_poSkeletonTracker->readFrame();

        std::vector<std::shared_ptr<OIUserData>> l_aoUsers = l_poFrame->getUsers();

#ifdef OPENISS_DEBUG
        std::cerr
            << "OpenISS::getSkeletons(): user count = "
            << l_aoUsers.size()
            << std::endl;
#endif

        for(const auto &l_oUser : l_aoUsers)
        {
            OISkeleton* l_poSkeleton = l_oUser->getSkeleton();

#ifdef OPENISS_DEBUG
            std::cerr << "l_poSkeleton = " << l_poSkeleton << std::endl;
#endif
            l_paoSkeletons.push_back(*l_poSkeleton);
        }
    }
    else
    {
        std::cerr
            << "OpenISS::getSkeletons(): the passed tracker is not a skeleton tracker..."
            << std::endl;
    }

#ifdef OPENISS_DEBUG
        std::cerr
            << "OpenISS::getSkeletons(): skeleton count = "
            << l_paoSkeletons.size()
            << std::endl;
#endif

    return l_paoSkeletons;
}

OIFace** OpenISS::getFaces(OITracker* p_poTracker)
{
    std::cerr << "OpenISS::getFaces() not implemented..." << std::endl;
    return nullptr;
}

std::vector<OIGestureData> OpenISS::getGestures(OITracker* p_poTracker)
{
    std::vector<OIGestureData> l_paoGestures;
    OIGestureTracker* l_poGestureTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bGestureTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getGestures(): gesture tracking is not enabled..."
            << std::endl;

        return l_paoGestures;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getGestures(): no suitable trackers found..."
            << std::endl;

        return l_paoGestures;
    }

    l_poGestureTracker = dynamic_cast<OIGestureTracker*>(l_poTracker);

    if(l_poGestureTracker != nullptr)
    {
        return l_poGestureTracker->getGestures();
    }
    else
    {
        std::cerr
            << "OpenISS::getGestures(): the passed tracker is not a gesture tracker..."
            << std::endl;
    }

    return l_paoGestures;
}

std::vector<OIHandData> OpenISS::getHands(OITracker* p_poTracker)
{
    std::vector<OIHandData> l_paoHands;
    OIGestureTracker* l_poGestureTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    if(this->m_bHandTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::getHands(): hand tracking is not enabled..."
            << std::endl;

        return l_paoHands;
    }

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::getHands(): no suitable trackers found..."
            << std::endl;

        return l_paoHands;
    }

    l_poGestureTracker = dynamic_cast<OIGestureTracker*>(l_poTracker);

    if(l_poGestureTracker != nullptr)
    {
        return l_poGestureTracker->getHands();
    }
    else
    {
        std::cerr
            << "OpenISS::getHands(): the passed tracker is not a hand/gesture tracker..."
            << std::endl;
    }

    return l_paoHands;
}

OIStatus OpenISS::enableDepth(bool p_bEnable, OIDevice* p_poDevice)
{
    if(p_bEnable)
    {
        if(p_poDevice != nullptr)
        {
            this->m_oSensorAdapter.reset(p_poDevice);
        }

        if(this->m_oSensorAdapter->enableDepth())
        {
            return OIStatus::STATUS_FAIL;
        }
    }
    else
    {
        // TODO: disable depth
        std::cerr << "OpenISS::enableDepth(false) not implemented..." << std::endl;
    }

    this->m_bDepthEnabled = p_bEnable;
    return OIStatus::STATUS_OK;
}

OIStatus OpenISS::enableRGB(bool p_bEnable, OIDevice* p_poDevice)
{
    if(p_bEnable)
    {
        if(p_poDevice != nullptr)
        {
            this->m_oSensorAdapter.reset(p_poDevice);
        }

        if(this->m_oSensorAdapter->enableColor() == false)
        {
            return OIStatus::STATUS_FAIL;
        }
    }
    else
    {
        // TODO: disable RGB
        std::cerr << "OpenISS::enableRGB(false) not implemented..." << std::endl;
    }

    this->m_bRGBEnabled = p_bEnable;
    return OIStatus::STATUS_OK;
}

OIStatus OpenISS::enableIR(bool p_bEnable, OIDevice* p_poDevice)
{
    if(p_bEnable)
    {
        if(p_poDevice != nullptr)
        {
            this->m_oSensorAdapter.reset(p_poDevice);
        }

        if(this->m_oSensorAdapter->enableRegistered())
        {
            return OIStatus::STATUS_FAIL;
        }
    }
    else
    {
        // TODO: disable IR
        std::cerr << "OpenISS::enableIR(false) not implemented..." << std::endl;
    }

    this->m_bIREnabled = p_bEnable;
    return OIStatus::STATUS_OK;
}

OIStatus OpenISS::enableSkeletonTracking(bool p_bEnable, OITracker* p_poTracker)
{
    OIStatus l_eStatus = OIStatus::STATUS_FAIL;

    if(p_bEnable == true && this->m_bSkeletonTrackingEnabled == true)
    {
        std::cerr
            << "OpenISS::enableSkeletonTracking(): skeleton tracking was already enabled..."
            << std::endl;

        return OIStatus::STATUS_OK;
    }

    if(p_bEnable == false && this->m_bSkeletonTrackingEnabled == false)
    {
        std::cerr
            << "OpenISS::enableSkeletonTracking(): skeleton tracking was already disabled..."
            << std::endl;

        return OIStatus::STATUS_OK;
    }

    OISkeletonTracker* l_poSkeletonTracker = nullptr;

    OITracker* l_poTracker = nullptr;

    // If the tracker in the parameter is not null we use it
    if(p_poTracker != nullptr)
    {
        l_poTracker = p_poTracker;
    }

    // Assume the first tracker in the list
    else if(this->m_paoTrackers.size() > 0)
    {
        l_poTracker = this->m_paoTrackers[0];
    }

    else
    {
        std::cerr
            << "OpenISS::enableSkeletonTracking(): no suitable trackers found..."
            << std::endl;

        return l_eStatus;
    }

    l_poSkeletonTracker = dynamic_cast<OISkeletonTracker*>(l_poTracker);

    if(l_poSkeletonTracker != nullptr)
    {
        if(p_bEnable)
        {
            l_poSkeletonTracker->startTracking();
            this->m_bSkeletonTrackingEnabled = true;
        }
        else
        {
            l_poSkeletonTracker->stopTracking();
            this->m_bSkeletonTrackingEnabled = false;
        }
    }
    else
    {
        std::cerr
            << "OpenISS::enableSkeletonTracking(): the passed tracker is not a skeleton tracker..."
            << std::endl;
    }

    return l_eStatus;
}

OIStatus OpenISS::enableGestureTracking(bool p_bEnable, OITracker* p_poTracker)
{
    OIStatus l_eStatus = OIStatus::STATUS_FAIL;

    OIGestureTracker* l_poGestureTracker = dynamic_cast<OIGestureTracker*>(p_poTracker);

    if(l_poGestureTracker != nullptr)
    {
        if(p_bEnable)
        {
            l_eStatus = l_poGestureTracker->startGestureDetection();

            if(l_eStatus == OIStatus::STATUS_OK)
            {
                this->m_bGestureTrackingEnabled = true;
            }
            else
            {
                std::cerr
                    << "OpenISS::enableGestureTracking(): could not start a gesture tracker"
                    << std::endl;
            }
        }
        else
        {
            l_eStatus = l_poGestureTracker->stopGestureDetection();
            this->m_bGestureTrackingEnabled = false;
        }
    }
    else
    {
        std::cerr
            << "OpenISS::enableGestureTracking(): tracker supplied is not a gesture tracker..."
            << std::endl;
    }

    return l_eStatus;
}

OIStatus OpenISS::enableHandTracking(bool p_bEnable, OITracker* p_poTracker)
{
    OIStatus l_eStatus = OIStatus::STATUS_FAIL;

    OIGestureTracker* l_poGestureTracker = dynamic_cast<OIGestureTracker*>(p_poTracker);

    if(l_poGestureTracker != nullptr)
    {
        if(p_bEnable)
        {
            l_eStatus = l_poGestureTracker->startHandTracking();

            if(l_eStatus == OIStatus::STATUS_OK)
            {
                this->m_bHandTrackingEnabled = true;
            }
            else
            {
                std::cerr
                    << "OpenISS::enableHandTracking(): could not start a hand tracker"
                    << std::endl;
            }
        }
        else
        {
            l_eStatus = l_poGestureTracker->stopHandTracking();
            this->m_bHandTrackingEnabled = false;
        }
    }
    else
    {
        std::cerr
            << "OpenISS::enableHandTracking(): tracker supplied is not a hand gesture tracker..."
            << std::endl;
    }

    return l_eStatus;

}

OIStatus OpenISS::enableFaceTracking(bool p_bEnable, OITracker* p_poTracker)
{
    std::cerr << "OpenISS::enableFaces() not implemented..." << std::endl;
    return OIStatus::STATUS_DEFAULT;
}

} // namespace

// EOF
