//
// COMP 371 Lab 6 - Models and EBOs
//
// Created by Zachary Lapointe on 17/07/2019.
//
#ifndef MAIN
#define MAIN

#include "OpenISS.hpp"

#include "opencv2/opencv.hpp"
#include "opencv2/imgproc/imgproc_c.h"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <algorithm>
#include <vector>

#define GLEW_STATIC 1   // This allows linking with Static Library on Windows, without DLL
#include <GL/glew.h>    // Include GLEW - OpenGL Extension Wrangler

#include <GLFW/glfw3.h> // GLFW provides a cross-platform interface for creating a graphical context,
                        // initializing OpenGL and binding inputs

#include <glm/glm.hpp>  // GLM is an optimized math library with syntax to similar to OpenGL Shading Language
#include <glm/gtc/matrix_transform.hpp> // include this to create transformation matrices
#include <glm/common.hpp>

#include "OBJloader.h"  //For loading .obj files
#include "OBJloaderV2.h"  //For loading .obj files using a polygon list format

#include "shaderloader.h"

#include "tools.cpp"
#include "unit_model.cpp"
#include "user_model.cpp"

using namespace glm;
using namespace std;
using namespace openiss;

// Context
OpenISS* g_oOpenISS = nullptr;

// Current device
OIDevice* g_oDevice = nullptr;

//GLOBALS
#define NORMAL_MODE 0
#define DEPTH_MODE 1
#define IR_MODE 2

// shader variable setters
void SetUniformMat4(GLuint shader_id, const char* uniform_name, mat4 uniform_value)
{
  glUseProgram(shader_id);
  glUniformMatrix4fv(glGetUniformLocation(shader_id, uniform_name), 1, GL_FALSE, &uniform_value[0][0]);
}

void SetUniformVec3(GLuint shader_id, const char* uniform_name, vec3 uniform_value)
{
  glUseProgram(shader_id);
  glUniform3fv(glGetUniformLocation(shader_id, uniform_name), 1, value_ptr(uniform_value));
}

template <class T>
void SetUniform1Value(GLuint shader_id, const char* uniform_name, T uniform_value)
{
  glUseProgram(shader_id);
  glUniform1i(glGetUniformLocation(shader_id, uniform_name), uniform_value);
  //glUseProgram(0); - removed this line to avoid turning of the shader program
}

// apply the transform to the given list of matrix
std::vector< UnitModel > apply_transform_2_models(std::vector< UnitModel > unitModelList, mat4 matrixTransform){
    std::vector< UnitModel > transformedUnitModelList;
    for(UnitModel um: unitModelList){
        um.transform(matrixTransform);
        transformedUnitModelList.push_back(um);
    }

    return transformedUnitModelList;
}

cv::Mat cam_mode(int mode, cv::Mat frame){

    cv::Mat greyMat;
    switch(mode){
        case NORMAL_MODE:
            {
                return frame;
            }

        case DEPTH_MODE:
            {
                cv::cvtColor(frame, greyMat, CV_BGR2GRAY);
                return greyMat;
            }

        case IR_MODE:
            {
                greyMat = cv::Mat(frame.rows, frame.cols, CV_8UC3);
                cv::cvtColor(frame, greyMat, CV_BGR2GRAY);
                cv::Mat coloredImage;
                cv::applyColorMap(greyMat, coloredImage, cv::COLORMAP_HSV);
                return coloredImage;
            }

        default:
            {
                return frame;
            }
    }
}

GLuint loadWebCamTexture(int mode, cv::Mat frame){
  GLuint textureId = 0;
  glGenTextures(1, &textureId);
  assert(textureId != 0);

	if (frame.data ==NULL) return -1;
	glBindTexture (GL_TEXTURE_2D, textureId);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  frame = cam_mode(mode, frame);

  if(mode == DEPTH_MODE){
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RED, frame.cols, frame.rows, 0, GL_RED, GL_UNSIGNED_BYTE, frame.data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, GL_RED);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, GL_RED);
  }
  else{
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, frame.cols, frame.rows, 0, GL_BGR, GL_UNSIGNED_BYTE, frame.data);
  }

	return textureId;
}

int main(int, char**) {

    g_oOpenISS = new OpenISS(NULL_DEVICE);

    // *** INIT CAM ***
    cv::VideoCapture camera(0);
    if (!camera.isOpened()) {
        std::cerr << "ERROR: Could not open camera" << std::endl;
    }
    // this will contain the image from the webcam
    cv::Mat frame;

    // *** INIT GL ***
    // Initialize GLFW and OpenGL version
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    // Create Window and rendering context using GLFW, resolution is 800x600
    GLFWwindow* window = glfwCreateWindow(1024, 768, "Comp371 - Quiz 2", NULL, NULL);
    if (window == NULL)
    {
        std::cerr << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to create GLEW" << std::endl;
        glfwTerminate();
        return -1;
    }

    // *** SETUP ***
    // Black background
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // setup shaders
    std::string shaderPathPrefix = "assets/shaders/";
    //GLuint shaderProgram = loadSHADER(shaderPathPrefix + "vertex.glsl", shaderPathPrefix + "frag.glsl");

    // new
    GLuint shaderProgram = loadSHADER(shaderPathPrefix + "scene_vertex.glsl", shaderPathPrefix + "scene_fragment.glsl");
    GLuint shaderShadow = loadSHADER(shaderPathPrefix + "shadow_vertex.glsl", shaderPathPrefix + "shadow_fragment.glsl");
    GLuint lightCubeShaderProgram = loadSHADER(shaderPathPrefix + "lightCube_vertex.glsl", shaderPathPrefix + "lightCube_frag.glsl");

    // Load Textures
    GLuint groundTextureID = loadTexture("assets/textures/ground.jpg");
    GLuint carpetTextureID = loadTexture("assets/textures/carpet.jpg");
    GLuint metalTextureID = loadTexture("assets/textures/metal.jpg");
    GLuint skyTextureID = loadTexture("assets/textures/sky.jpg");

    GLuint whiteTextureID = loadTexture("assets/textures/white.jpg");
    GLuint purpleTextureID = loadTexture("assets/textures/purple.jpg");
    GLuint greenTextureID = loadTexture("assets/textures/green.png");
    GLuint pinkTextureID = loadTexture("assets/textures/pink.jpg");
    GLuint blueTextureID = loadTexture("assets/textures/blue.png");
    GLuint yellowTextureID = loadTexture("assets/textures/yellow.jpg");
    GLuint orangeTextureID = loadTexture("assets/textures/orange.jpg");

    GLuint screenTextureID = loadTexture("assets/textures/orange.jpg");

    //Setup models
    string cubePath = "assets/models/cube_git.obj";

    //TODO 1 load the more interesting model: "heracles.obj"
    //TODO 3 load the models as EBOs instead of only VBOs

    int cubeVertices;
    GLuint cubeVAO = setupModelEBO(cubePath, cubeVertices);

    int activeVAOVertices = cubeVertices;
    GLuint activeVAO = cubeVAO;

    // new
    // ** SHADOW **
    // Setup texture and framebuffer for creating shadow map

    // Dimensions of the shadow texture, which should cover the viewport window size and shouldn't be oversized and waste resources
    //const unsigned int DEPTH_MAP_TEXTURE_SIZE = 1024;

    //// Variable storing index to texture used for shadow mapping
    //GLuint depth_map_texture;
    //// Get the texture
    //glGenTextures(1, &depth_map_texture);
    //// Bind the texture so the next glTex calls affect it
    //glBindTexture(GL_TEXTURE_2D, depth_map_texture);
    //// Create the texture and specify it's attributes, including widthn height, components (only depth is stored, no color information)
    //glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, DEPTH_MAP_TEXTURE_SIZE, DEPTH_MAP_TEXTURE_SIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT,
               //NULL);
    //// Set texture sampler parameters.
    //// The two calls below tell the texture sampler inside the shader how to upsample and downsample the texture. Here we choose the nearest filtering option, which means we just use the value of the closest pixel to the chosen image coordinate.
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    //// The two calls below tell the texture sampler inside the shader how it should deal with texture coordinates outside of the [0, 1] range. Here we decide to just tile the image.
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    //glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);


    // Variable storing index to framebuffer used for shadow mapping
    //GLuint depth_map_fbo;  // fbo: framebuffer object
    //// Get the framebuffer
    //glGenFramebuffers(1, &depth_map_fbo);
    //// Bind the framebuffer so the next glFramebuffer calls affect it
    //glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
    //// Attach the depth map texture to the depth map framebuffer
    ////glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, depth_map_texture, 0);
    //glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depth_map_texture, 0);
    //glDrawBuffer(GL_NONE); //disable rendering colors, only write depth values

    // NEW: Set the textures in the main shader program
    //glActiveTexture(GL_TEXTURE0); // This is active by default since it is the default texture object (like a picture frame).
    //glBindTexture(GL_TEXTURE_2D, depth_map_texture); // We bind the shadow texture to object 0.
    //SetUniform1Value(shaderProgram, "shadow_map", 0); // This number corresponts to a texture object, in this case 0.

    //glActiveTexture(GL_TEXTURE1); // This is the next texture object. It can be written as GL_TEXTURE0 + 1 as well.
    //glBindTexture(GL_TEXTURE_2D, metalTextureID); //  We bind the brick texture to object 1.
    //SetUniform1Value(shaderProgram, "textureSampler", 1);

    // If we want to change one of these textures we call the glActiveTexture,
    // followed by glBindTexture.
    // If we want to change the which object is being pointed to in the shader
    // progrram we have to change the uniform value
    
    // ** SHADOW **

    // Camera parameters for view transform
    vec3 cameraPosition(0.0f,2.0f,2.0f);
    vec3 cameraLookAt(0.0f, 0.0f, 0.0f);
    vec3 cameraUp(0.0f, 1.0f, 0.0f);

    // Other camera parameters
    float cameraSpeed = 1.0f;
    float cameraFastSpeed = 10 * cameraSpeed;
    float cameraHorizontalAngle = 90.0f;
    float cameraVerticalAngle = 0.0f;

    float cameraRotationAngle = 0.0f;
    float cameraRadiusPos = 5.0f;

    // Set projection matrix for shader, this won't change
    mat4 projectionMatrix = glm::perspective(70.0f,            // field of view in degrees
                                             800.0f / 600.0f,  // aspect ratio
                                             0.01f, 100.0f);   // near and far (near > 0)

    // Set initial view matrix
    mat4 viewMatrix = lookAt(cameraPosition,  // eye
                             cameraPosition + cameraLookAt,  // center
                             cameraUp ); // up

    // new
    // Set View and Projection matrices on both shaders
    // Set shader variables
    SetUniform1Value(shaderProgram, "shadow_map", 0);
    SetUniform1Value(shaderProgram, "textureSampler", 1);

    // Set projection matrix on both shaders
    SetUniformMat4(shaderProgram, "projectionMatrix", projectionMatrix);

    // Set view matrix on both shaders
    SetUniformMat4(shaderProgram, "viewMatrix", viewMatrix);

    float lightAngleOuter = 30.0;
    float lightAngleInner = 20.0;
    // Set light cutoff angles on scene shader
    SetUniform1Value(shaderProgram, "light_cutoff_inner", cos(radians(lightAngleInner)));
    SetUniform1Value(shaderProgram, "light_cutoff_outer", cos(radians(lightAngleOuter)));

    // Set object color on scene shader
    SetUniformVec3(shaderProgram, "object_color", vec3(1.0, 1.0, 1.0));
    //new

    // For frame time
    float lastFrameTime = glfwGetTime();
    int lastMouseLeftState = GLFW_RELEASE;
    double lastMousePosX, lastMousePosY;
    glfwGetCursorPos(window, &lastMousePosX, &lastMousePosY);

    // Other OpenGL states to set once
    // Enable Backface culling
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_NORMALIZE); // helps with some lighting issues
    //https://www.opengl.org/archives/resources/features/KilgardTechniques/oglpitfall/

    // Init Letter Models
    std::vector< LetterIDModel > list_letter_id;

    int segO[] = {1,1,1,1,1,1,0};
    LetterIDModel OList = LetterIDModel(segO, shaderProgram, activeVAOVertices, activeVAO, whiteTextureID);
    list_letter_id.push_back(OList);

    int segP[] = {1,1,0,0,1,1,1};
    LetterIDModel PList = LetterIDModel(segP, shaderProgram, activeVAOVertices, activeVAO, purpleTextureID);
    list_letter_id.push_back(PList);

    int segH[] = {0,1,1,0,1,1,1};
    LetterIDModel HList = LetterIDModel(segH, shaderProgram, activeVAOVertices, activeVAO, greenTextureID);
    list_letter_id.push_back(HList);

    int segI[] = {0,0,0,0,1,1,0};
    LetterIDModel IList = LetterIDModel(segI, shaderProgram, activeVAOVertices, activeVAO, pinkTextureID);
    list_letter_id.push_back(IList);

    int segL[] = {0,0,0,1,1,1,0};
    LetterIDModel LList = LetterIDModel(segL, shaderProgram, activeVAOVertices, activeVAO, whiteTextureID);
    list_letter_id.push_back(LList);

    int segI2[] = {0,0,0,0,1,1,0};
    LetterIDModel IList2 = LetterIDModel(segI2, shaderProgram, activeVAOVertices, activeVAO, yellowTextureID);
    list_letter_id.push_back(IList2);
    
    //int segTest[] = {1,1,1,0,0,0,0};
    //LetterIDModel test = LetterIDModel(segTest, shaderProgram, activeVAOVertices, activeVAO, yellowTextureID);
    //list_letter_id.push_back(test);

    // Init Parameters
    int numLetterID = 6;
    vec3 mainLightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 cameraCircleLightColor = vec3(1.0f, 1.0f, 1.0f);
    vec3 spotLightColor = vec3(1.0f, 1.0f, 1.0f);
    bool mainLightFlag = true;
    vec3 cameraCircleLightPosition = vec3(0.0f,3.0f, 5.0f); // the location of the light in 3D space

    // webcam init.
    int camMode = DEPTH_MODE;

    // Entering Main Loop
    while(!glfwWindowShouldClose(window))
    {
        // update the webcam frame
        camera >> frame;
        screenTextureID = loadWebCamTexture(camMode, frame);
        //screenTextureID = loadWebCamTextureNEW(camMode, frame);

        // ** LIGHT POSITION AND ANGLE **
        // light parameters
        vec3 lightFocus(0.0, 0.0, 1.0);      // the point in 3D space the light "looks" at

        vec3 mainLightPosition =  vec3(0.0f, 0.0f, 1.0f); // the location of the light in 3D space
        //vec3(sinf(glfwGetTime() * 6.0f * 3.141592f), sinf(glfwGetTime() * 3.141592f), cosf(glfwGetTime() * 3.141592f));
        //vec3(sinf(glfwGetTime() * 6.0f * 3.141592f), 3.0f, 10.0f);

        vec3 spotlightLocation = vec3(0.0f, 5.0f, 5.0f);
        vec3 spotLightPosition = spotlightLocation;

        vec3 mainLightDirection = normalize(lightFocus - mainLightPosition);
        vec3 cameraCircleLightDirection = normalize(lightFocus - cameraCircleLightPosition);
        vec3 spotLightDirection = normalize(lightFocus - spotLightPosition);

        float lightNearPlane = 10.0f;
        float lightFarPlane = 180.0f;

        mat4 lightProjectionMatrix = frustum(-1.0f, 1.0f, -1.0f, 1.0f, lightNearPlane, lightFarPlane);
        //const unsigned int DEPTH_MAP_TEXTURE_SIZE = 1024;
        //perspective(20.0f, (float)DEPTH_MAP_TEXTURE_SIZE / (float)DEPTH_MAP_TEXTURE_SIZE, lightNearPlane, lightFarPlane);
        
        
        // ** LIGHT POSITION AND ANGLE **
        // ** LIGHT SETUP **
        mat4 lightViewMatrix = lookAt(mainLightPosition + cameraCircleLightPosition + spotLightPosition, lightFocus, vec3(0.0f, 1.0f, 0.0f));
        mat4 lightSpaceMatrix = lightProjectionMatrix * lightViewMatrix;

        // Set light space matrix on both shaders
        SetUniformMat4(shaderShadow, "light_view_proj_matrix", lightSpaceMatrix);
        SetUniformMat4(shaderProgram, "light_view_proj_matrix", lightSpaceMatrix);

        // Set light far and near planes on scene shader
        SetUniform1Value(shaderProgram, "light_near_plane", lightNearPlane);
        SetUniform1Value(shaderProgram, "light_far_plane", lightFarPlane);

        // Set light position on scene shader
        SetUniformVec3(shaderProgram, "pointLights[0].light_position", mainLightPosition);
        SetUniformVec3(shaderProgram, "pointLights[1].light_position", cameraCircleLightPosition);
        SetUniformVec3(shaderProgram, "pointLights[2].light_position", spotLightPosition);

        SetUniformVec3(shaderProgram, "pointLights[0].light_color", mainLightColor);
        SetUniformVec3(shaderProgram, "pointLights[1].light_color", cameraCircleLightColor);
        SetUniformVec3(shaderProgram, "pointLights[2].light_color", spotLightColor);

        // Set light direction on scene shader
        SetUniformVec3(shaderProgram, "pointLights[0].light_direction", mainLightDirection);
        SetUniformVec3(shaderProgram, "pointLights[1].light_direction", cameraCircleLightDirection);
        SetUniformVec3(shaderProgram, "pointLights[2].light_direction", spotLightDirection);
        // ** LIGHT POSITION AND ANGLE **
        //new

        // Frame time calculation
        float dt = glfwGetTime() - lastFrameTime;
        lastFrameTime += dt;

        // Each frame, reset color of each pixel to glClearColor
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Draw colored geometry
        glUseProgram(shaderProgram);

        // ** CREATE MODELS **
        // skybox
        mat4 skyboxMatrix =
            glm::translate(mat4(1.0f), vec3(0.0f, 0.0f, 0.0f)) *
            glm::scale(mat4(1.0f), vec3(50.0f, 50.0f, 50.0f));
        UnitModel groundModel = UnitModel(shaderProgram, skyboxMatrix, activeVAOVertices, activeVAO, skyTextureID);
        // ground
        mat4 groundMatrix =
            glm::translate(mat4(1.0f), vec3(0.0f, 0.0f, 0.0f)) *
            glm::scale(mat4(1.0f), vec3(5.0f, 0.01f, 5.0f));
        UnitModel skyboxModel = UnitModel(shaderProgram, groundMatrix, activeVAOVertices, activeVAO, groundTextureID);

        // stage
        std::vector<UnitModel> stageList;
        int gridUnit = 1;
        float lengthStageFloor = 1;
        float widthStageFloor  = 0.7;
        float heightStageFloor = 0.1;

        float lengthStageFloorSide = 0.3;
        float widthStageFloorSide  = 0.9;
        float heightStageFloorSide  = 0.05;

        mat4 og_stageMiddleFloorMatrix = translate(mat4(1.0f), vec3(0.0f , 0.0f, 0.0f )) *  scale(mat4(1.0f), vec3(lengthStageFloor, heightStageFloor, widthStageFloor));
        UnitModel stageMiddleFloor  = UnitModel(shaderProgram, og_stageMiddleFloorMatrix, activeVAOVertices, activeVAO, carpetTextureID);
        stageList.push_back(stageMiddleFloor);

        mat4 og_stageLeftFloorMatrix = translate(mat4(1.0f), vec3(-1.0f , 0.0f, 0.2f)) *  scale(mat4(1.0f), vec3(lengthStageFloorSide, heightStageFloor, widthStageFloorSide));
        UnitModel stageLeftFloor   = UnitModel(shaderProgram, og_stageLeftFloorMatrix, activeVAOVertices, activeVAO, carpetTextureID);
        stageList.push_back(stageLeftFloor);

        mat4 og_stageRightFloorMatrix = translate(mat4(1.0f), vec3(1.0f , 0.0f, 0.2f)) *  scale(mat4(1.0f), vec3(lengthStageFloorSide, heightStageFloor, widthStageFloorSide));
        UnitModel stageRightFloor   = UnitModel(shaderProgram, og_stageRightFloorMatrix, activeVAOVertices, activeVAO, carpetTextureID);
        stageList.push_back(stageRightFloor);

        // Make Axis
        std::vector<UnitModel> axisList;
        float lengthAxis = gridUnit * 0.7 / 2;
        float radiusAxis = gridUnit * 0.07 / 2;

        mat4 og_xAxisCylinderMatrix = translate(mat4(1.0f), vec3(lengthAxis/2 , 0.0f, 0.0f)) * rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f)) * scale(mat4(1.0f), vec3(radiusAxis, lengthAxis, radiusAxis));
        mat4 og_yAxisCylinderMatrix = translate(mat4(1.0f), vec3(0.0f , lengthAxis/2, 0.0f)) * scale(mat4(1.0f), vec3(radiusAxis, lengthAxis, radiusAxis));
        mat4 og_zAxisCylinderMatrix = translate(mat4(1.0f), vec3(0.0f , 0.0f, lengthAxis/2)) * rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f)) * scale(mat4(1.0f), vec3(radiusAxis, lengthAxis, radiusAxis));

        UnitModel xAxisCylinder = UnitModel(shaderProgram, og_xAxisCylinderMatrix, activeVAOVertices, activeVAO, metalTextureID);
        UnitModel yAxisCylinder = UnitModel(shaderProgram, og_yAxisCylinderMatrix, activeVAOVertices, activeVAO, greenTextureID);
        UnitModel zAxisCylinder = UnitModel(shaderProgram, og_zAxisCylinderMatrix, activeVAOVertices, activeVAO, blueTextureID);

        axisList.push_back(xAxisCylinder);
        axisList.push_back(yAxisCylinder);
        axisList.push_back(zAxisCylinder);

        // totem model
        // model letter/id rotations & specific transformations for focused model
        float gapSize = 0.4f;
        for(int i = 0; i < numLetterID; i++){
            mat4 scaleMatrix = scale(mat4(1.0f), vec3(list_letter_id[i].scale, list_letter_id[i].scale, list_letter_id[i].scale));
            mat4 moveMatrix = translate(mat4(1.0f), vec3(list_letter_id[i].x,list_letter_id[i].y + 0.2f + 0.2f,list_letter_id[i].z));
            mat4 rotateMatrix = rotate(glm::mat4(1.0f), glm::radians(list_letter_id[i].angle), glm::vec3(0.0f, 1.0f, 0.0f));

            mat4 translateMatrix = translate(mat4(1.0f), vec3(0.0f, gapSize * i, 0.0f)); // move to the back of the grid and stack up

            list_letter_id[i].m_model_list = apply_transform_2_models(list_letter_id[i].m_model_list, translateMatrix * moveMatrix * scaleMatrix * rotateMatrix );
        }

        // spotlight model rod
        mat4 spotlightRodMatrix =
            glm::translate(mat4(1.0f), spotlightLocation) *
            glm::scale(mat4(1.0f), vec3(0.1f, 0.5f, 0.1f));
        UnitModel spotlightRod = UnitModel(shaderProgram, spotlightRodMatrix, activeVAOVertices, activeVAO, metalTextureID);

        // stage screen
        mat4 stageScreenMatrix =
            glm::translate(mat4(1.0f), vec3(0,0.5f,0)) *
            rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f)) *
            glm::scale(mat4(1.0f), vec3(0.2f, 0.5f, 1.0f));
        UnitModel stageScreen = UnitModel(shaderProgram, stageScreenMatrix, activeVAOVertices, activeVAO, screenTextureID);

        // move axis
        axisList = apply_transform_2_models(axisList, translate(mat4(1.0f), vec3(0.0f, 0.3f, 5.0f)));

        // ** DRAW MODELS **
        skyboxModel.draw();
        groundModel.draw();
        spotlightRod.draw();
        stageScreen.draw();
        draw_models(stageList);
        draw_models(axisList);

        //for(LetterIDModel & lim: list_letter_id){
            //draw_models(lim.m_model_list);
        //}
        // NOTE : Very Important ! When doing this kind of for loop, you have to use "REFERENCE", not like in python ...
        

        // Set the view matrix for first person camera
        mat4 viewMatrix(1.0f);
        viewMatrix = lookAt(cameraPosition, cameraPosition + cameraLookAt, cameraUp);
        setViewMatrix(shaderProgram, viewMatrix);

        //new
        // Set view position on scene shader
        SetUniformVec3(shaderProgram, "view_position", cameraPosition);
        //new
        
        //new
        // ** SHADOW **
        // Render shadow in 2 passes: 1- Render depth map, 2- Render scene
        // 1- Render shadow map:
        // a- use program for shadows
        // b- resize window coordinates to fix depth map output size
        // c- bind depth map framebuffer to output the depth values
        //{
          //// Use proper shader
          //glUseProgram(shaderShadow);
          //// Use proper image output size
          //glViewport(0, 0, DEPTH_MAP_TEXTURE_SIZE, DEPTH_MAP_TEXTURE_SIZE);
          //// Bind depth map texture as output framebuffer
          //glBindFramebuffer(GL_FRAMEBUFFER, depth_map_fbo);
          //// Clear depth data on the framebuffer
          //glClear(GL_DEPTH_BUFFER_BIT);
            ////glActiveTexture(GL_TEXTURE0);
            ////glBindTexture(GL_TEXTURE_2D, depth_map_texture);
          //// Bind geometry
          //glBindVertexArray(activeVAO);
          //// Draw geometry
          //glDrawElements(GL_TRIANGLES, activeVAOVertices, GL_UNSIGNED_INT, 0);
          //// Unbind geometry
          //glBindVertexArray(0);
        //}

         ////2- Render scene: a- bind the default framebuffer and b- just render like what we do normally
        //{
          //// Use proper shader
          //glUseProgram(shaderProgram);
          //// Use proper image output size
          //// Side note: we get the size from the framebuffer instead of using WIDTH and HEIGHT because of a bug with highDPI displays
          //int width, height;
          //glfwGetFramebufferSize(window, &width, &height);
          //glViewport(0, 0, width, height);
          //// Bind screen as output framebuffer
          //glBindFramebuffer(GL_FRAMEBUFFER, 0);
          //// Clear color and depth data on framebuffer
          //glClearColor(0.8f, 0.8f, 0.8f, 1.0f);
          //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
          //// Bind depth map texture: not needed, by default it is active
            ////glActiveTexture(GL_TEXTURE0);
            ////glBindTexture(GL_TEXTURE_2D, depth_map_texture);
            ////glActiveTexture(GL_TEXTURE1);
            ////glBindTexture(GL_TEXTURE_2D, bricks);
          //// Bind geometry
          //glBindVertexArray(activeVAO);
          //// Draw geometry
          //glDrawElements(GL_TRIANGLES, activeVAOVertices, GL_UNSIGNED_INT, 0);
          //// Unbind geometry
          //glBindVertexArray(0);
        //}
        // ** SHADOW **
        //new
        
        // draw lightCube
        glUseProgram(lightCubeShaderProgram);
        //setup light cube
        //set shader
        glUniformMatrix4fv(glGetUniformLocation(lightCubeShaderProgram, "projectionMatrix"), 1, GL_FALSE, &projectionMatrix[0][0]);
        glUniformMatrix4fv(glGetUniformLocation(lightCubeShaderProgram, "viewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);

        //mat4 lightCubeMatrix = translate(mat4(1.0f), vec3(0.0f, 0.0f, 2.0f)) * scale(mat4(1.0f), vec3(0.1f, 0.1f, 0.1f));
        //LightCube lightCube = LightCube(lightCubeShaderProgram, lightCubeMatrix, activeVAOVertices, activeVAO);
        //lightCube.draw();

        mat4 spotlightCubeMatrix = translate(mat4(1.0f), spotlightLocation - vec3(0.0f, 0.33f, 0.0f)) * rotate(glm::mat4(1.0f), glm::radians(70.0f), glm::vec3(1.0f, 0.0f, 0.0f)) * scale(mat4(1.0f), vec3(0.15f, 0.3f, 0.15f));
        LightCube spotlightCube = LightCube(lightCubeShaderProgram, spotlightCubeMatrix, activeVAOVertices, activeVAO);
        spotlightCube.draw();
        
        // End Frame
        glfwSwapBuffers(window);
        glfwPollEvents();

        // ** INPUTS **
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, true);

        // This was solution for Lab02 - Moving camera exercise
        // We'll change this to be a first or third person camera
        bool fastCam = glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS;
        float currentCameraSpeed = (fastCam) ? cameraFastSpeed : cameraSpeed;

        // - Calculate mouse motion dx and dy
        // - Update camera horizontal and vertical angle
        double mousePosX, mousePosY;
        glfwGetCursorPos(window, &mousePosX, &mousePosY);

        double dx = mousePosX - lastMousePosX;
        double dy = mousePosY - lastMousePosY;

        lastMousePosX = mousePosX;
        lastMousePosY = mousePosY;


        // Convert to spherical coordinates
        const float cameraAngularSpeed = 5.0f;
        cameraHorizontalAngle -= dx * cameraAngularSpeed * dt;
        cameraVerticalAngle   -= dy * cameraAngularSpeed * dt;

        // Clamp vertical angle to [-85, 85] degrees
        cameraVerticalAngle = std::max(-85.0f, std::min(85.0f, cameraVerticalAngle));

        float theta = radians(cameraHorizontalAngle);
        float phi = radians(cameraVerticalAngle);

        cameraLookAt = vec3(cosf(phi)*cosf(theta), sinf(phi), -cosf(phi)*sinf(theta));
        //cameraLookAt = vec3(0.0f, 0.0f, 0.0f);
        vec3 cameraSideVector = glm::cross(cameraLookAt, vec3(0.0f, 1.0f, 0.0f));

        glm::normalize(cameraSideVector);

        // Use camera lookat and side vectors to update positions with ASDW
        if (glfwGetKey(window, GLFW_KEY_W ) == GLFW_PRESS)
        {
            cameraPosition += cameraLookAt * dt * currentCameraSpeed;
        }

        if (glfwGetKey(window, GLFW_KEY_S ) == GLFW_PRESS)
        {
            cameraPosition -= cameraLookAt * dt * currentCameraSpeed;
        }

        if (glfwGetKey(window, GLFW_KEY_D ) == GLFW_PRESS)
        {
            cameraPosition += cameraSideVector * dt * currentCameraSpeed;
        }

        if (glfwGetKey(window, GLFW_KEY_A ) == GLFW_PRESS)
        {
            cameraPosition -= cameraSideVector * dt * currentCameraSpeed;
        }


        // main model camera
        if (glfwGetKey(window, GLFW_KEY_R ) == GLFW_PRESS)
        {
            vec3 pos(0.0f,1.0f,5.0f);
            cameraPosition = pos;

            cameraHorizontalAngle = 90.0f;
            cameraVerticalAngle = 0.0f;
        }

        // front model camera
        if (glfwGetKey(window, GLFW_KEY_M ) == GLFW_PRESS)
        {
            vec3 pos(0.0f,2.0f,2.0f);
            cameraPosition = pos;

            cameraHorizontalAngle = 90.0f;
            cameraVerticalAngle = 0.0f;
        }

        // back model camera
        if (glfwGetKey(window, GLFW_KEY_B ) == GLFW_PRESS)
        {
            vec3 pos(0.0f,2.0f,-2.0f);
            cameraPosition = pos;

            cameraHorizontalAngle = -90.0f;
            cameraVerticalAngle = 0.0f;
        }
        
        // circling camera
        if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
        {
            cameraRotationAngle += 0.1f;

            if(cameraRotationAngle >= 359){
                cameraRotationAngle = 359;
            }

            int cameraXPos = sinf(cameraRotationAngle) * cameraRadiusPos;
            int cameraZPos = cosf(cameraRotationAngle) * cameraRadiusPos;

            vec3 pos(cameraXPos,3.0f, cameraZPos);
            cameraPosition = pos;

            cameraLookAt = cameraPosition * -1.0f;

            // light position
            cameraCircleLightPosition = pos;

        }
        
        if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) 
        {

            cameraRotationAngle -= 0.1f;

            if(cameraRotationAngle <= 0){
                cameraRotationAngle = 0;
            }

            int cameraXPos = sinf(cameraRotationAngle) * cameraRadiusPos;
            int cameraZPos = cosf(cameraRotationAngle) * cameraRadiusPos;

            vec3 pos(cameraXPos,3.0f, cameraZPos);
            cameraPosition = pos;

            cameraLookAt = cameraPosition * -1.0f;

            cameraCircleLightPosition = pos;
        }

        // camera at main light position
        if (glfwGetKey(window, GLFW_KEY_X ) == GLFW_PRESS)
        {
            cameraPosition = mainLightPosition;

            cameraHorizontalAngle = 90.0f;
            cameraVerticalAngle = 0.0f;
        }
        // turn on/off main light
        //if (glfwGetKey(window, GLFW_KEY_1 ) == GLFW_PRESS)
        //{
            //mainLightColor = vec3(1.0f, 1.0f, 1.0f);
            //cameraCircleLightColor = vec3(1.0f, 1.0f, 1.0f);
            //spotLightColor = vec3(1.0f, 1.0f, 1.0f);
        //}
        //if (glfwGetKey(window, GLFW_KEY_2 ) == GLFW_PRESS)
        //{
            //mainLightColor = vec3(0.0f, 0.0f, 0.0f);
            //cameraCircleLightColor = vec3(0.0f, 0.0f, 0.0f);
            //spotLightColor = vec3(0.0f, 0.0f, 0.0f);
        //}

        // Webcam Mode
        if (glfwGetKey(window, GLFW_KEY_1 ) == GLFW_PRESS)
        {
            camMode = NORMAL_MODE;
        }
        if (glfwGetKey(window, GLFW_KEY_2 ) == GLFW_PRESS)
        {
            camMode = DEPTH_MODE;
        }
        if (glfwGetKey(window, GLFW_KEY_3 ) == GLFW_PRESS)
        {
            camMode = IR_MODE;
        }
    }

    glfwTerminate();
    return 0;
}

#endif
